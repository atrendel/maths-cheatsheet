\section{Séries réelles ou complexes}


\subsection{Sommation par paquets}

Soit $\sum\limits u_n$ une série réelle ou complexe convergente, $(p_n)_{n \geq 1} \in \mathbb{N}^{\mathbb{N}}$ strictement croissante. Notons $ \sigma_n = \sum\limits_{k=p_n}^{p_{n+1}-1} u_k$.

Alors $\sum\limits \sigma_n$ converge et $\sum\limits_{n=1}^{+\infty} \sigma_n = \sum\limits_{n=1}^{+\infty} u_n$.

Réciproquement, si les éléments de chaque paquet ont le même signe et $\sum\limits \sigma_n$ converge, alors $\sum\limits u_n$ converge.


\subsection{Théorème de Cesaro}

Soit $(u_n)$ une suite de réels ou de complexes convergente. 

Alors $\lim\limits_{n\to\infty} u_n = \lim\limits_{n\to\infty} \frac{1}{n} \sum\limits_{k=1}^n u_k$.


\subsection{Séries particulières}

\paragraph{Séries de Riemann.} $\sum\limits \frac{1}{n^\alpha}$ converge \textit{ssi} $\alpha > 1$.

\paragraph{Démonstration.} Montrer que, pour $(u_n)$ est une suite de réels positifs \textit{décroissante}, $\sum\limits u_n$ et $\sum\limits 2^n u_{2^n}$ ont même nature. On peut alors se ramener à une série géométrique.

\paragraph{Séries de Bertrand.} $\sum\limits \frac{1}{n^\alpha \ln^\beta n}$ converge \textit{ssi} $\alpha > 1$ ou $\alpha = 1$ et $\beta > 1$.


\subsection{Comparaison des séries à termes positifs}
\label{sec:reg}

\paragraph{Quelques rappels.} On a les équivalences
\begin{align*}
	u_n = O(v_n) &\Leftrightarrow \exists M \geq 0, \exists N \in \mathbb{N} \st \forall n \geq N, |u_n| \leq M |v_n| \\
	u_n = o(v_n) &\Leftrightarrow \forall \varepsilon > 0, \exists N \in \mathbb{N} \st \forall n \geq N, |u_n| \leq \varepsilon |v_n| \\
	u_n \sim v_n &\Leftrightarrow u_n - v_n = o(u_n) = o(v_n).
\end{align*}

\paragraph{} Dans la suite, $\sum u_n$ et $\sum v_n$ sont deux séries \textit{à termes positifs}.

\paragraph{Première règle.} Si $u_n \leq v_n$ à partir d'un certain rang, alors
\begin{enumerate}
	\item si $\sum v_n$ converge, $\sum u_n$ converge;
	\item si $\sum u_n$ diverge, $\sum v_n$ diverge.
\end{enumerate}

Même énoncé si $u_n = O(v_n), u_n = o(v_n)$, ou encore $u_n \sim v_n$.

\paragraph{Sommation.} Si $u_n = O(v_n)$,
\begin{enumerate}
	\item si $\sum v_n$ converge, $\sum\limits_{k=n+1}^{+\infty} u_k = O \left( \sum\limits_{k=n+1}^{+\infty} v_k \right)$;
	\item si $\sum v_n$ diverge, $\sum\limits_{k=1}^{n} u_k = O \left( \sum\limits_{k=1}^{n} v_k \right)$.
\end{enumerate}

De même pour les cas $u_n = o(v_n)$, ou encore $u_n \sim v_n$.

\paragraph{Deuxième règle.} Si $\frac{u_{n+1}}{u_n} \leq \frac{v_{n+1}}{v_n}$ à partir d'un certain rang, alors on a les mêmes conclusions.


\subsection{Critères de convergence}

\paragraph{Règle de Cauchy.} Soit $\sum\limits u_n$ une série \textit{à termes positifs} telle qu'il existe $l \in \overline{\mathbb{R}_+} \st \sqrt[n]{u_n} \to l$. 

Alors si $l < 1$, $\sum\limits u_n$ converge et si $l > 1$, $\sum\limits u_n$ diverge.

\paragraph{Démonstration.} Exploiter l'espace entre $l$ et $1$.

\paragraph{Règle de d'Alembert.} Soit $\sum\limits u_n$ une série \textit{à termes positifs} telle qu'il existe $l \in \overline{\mathbb{R}_+} \st \frac{u_{n+1}}{u_n} \to l$. 

Alors si $l < 1$, $\sum\limits u_n$ converge et si $l > 1$, $\sum\limits u_n$ diverge.

\paragraph{Démonstration.} Exploiter l'espace entre $l$ et $1$ et la deuxième règle de comparaison.

\paragraph{Règle de Riemann.} Soit $\sum\limits u_n$ une série à termes positifs telle qu'il existe $l \in \overline{\mathbb{R}_+} \st n^\alpha u_n \to l$.

Alors:
\begin{enumerate}
	\item si $l \in \mathbb{R}_+^*$, $\sum\limits u_n$ et $\sum\limits \frac{1}{n^\alpha}$ sont de même nature;
	\item si $l = 0$ et $\alpha > 1$, $\sum\limits u_n$ converge;
	\item si $l = +\infty$ et $\alpha \leq 1$ $\sum\limits u_n$ diverge.
\end{enumerate} 


\subsection{Critère spécial des séries alternées}

Soit $\sum\limits u_n$ une série alternée telle que $u_n \to 0$ et $(|u_n|)$ décroissante.

Alors $\sum\limits u_n$ converge, $\left| \sum\limits_{n=1}^{+\infty} u_n \right| \leq |u_1|$ et $\sum\limits_{n=1}^{+\infty} u_n$ et $u_1$ sont de même signe.

\paragraph{Démonstration.} Avec sous séries complémentaires.


\subsection{Constante d'Euler}

La suite $u_n = \sum\limits_{k=1}^n \frac{1}{k} - \ln n$ converge, sa limite est notée $\gamma$ et est appelée constante d'Euler.

\paragraph{Démonstration.} Utiliser les accroissements finis pour montrer que $\forall n \geq 1, \frac{1}{n+1} \leq \ln (n+1) - \ln (n) \leq \frac{1}{n}$. Sommer cette inégalité pour minorer la suite.


\subsection{Formule de Stirling}

Les \textbf{intégrales de Wallis} $I_n$ sont définies par $I_n = \int_0^{\frac{\pi}{2}} \cos^n x \ud x = \int_0^{\frac{\pi}{2}} \sin^n x \ud x$. On montre que $I_n \sim \sqrt{\frac{\pi}{2n}}$.

\paragraph{Démonstration.} Procéder par intégration par parties pour trouver une relation entre $I_n$ et $I_{n-2}$. En déduire $I_{2n}$ et $I_{2n+1}$. Montrer que $(I_n)$ décroît et $I_n \sim I_{n+1}$, et que $((n+1)I_nI_{n+1})$ est constante, conclure.

\paragraph{} On peut montrer à l'aide de ce résultat l'équivalent suivant, c'est la \textbf{formule de Stirling}: $n! \sim \left( \frac{n}{e} \right)^n \sqrt{2 \pi n} $.


\subsection{Produit de Cauchy}

La série $\sum\limits_{n \geq 0} \sum\limits_{j+k=n} u_j v_k = \sum\limits_{n \geq 0} \sum\limits_{j=0}^n u_j v_{n-j}$ est le \textbf{produit de Cauchy} de $\sum\limits u_n$ et $\sum\limits v_n$ (séries réelles ou complexes).

Le produit de Cauchy de deux séries absolument convergentes est absolument convergent et la somme du produit de Cauchy est le produit des sommes.

\paragraph{Démonstration.} Avec des carrés et des triangles...