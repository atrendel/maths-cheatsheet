\section{Algèbre générale}


\subsection{Relations d'équivalences}

Soient $E$ un ensemble non vide et $(A_i)_{i\in I}$ une partion de $E$. La relation $\mathscr{R}$ définie sur $E$ par $x\mathscr{R}y \Leftrightarrow \exists i \in I \st \{x, y\} \subset A_i$ est une \textbf{relation d'équivalence} (réflexive, symétrique, transitive).

Les $A_i$ sont appelées les \textbf{classes d'équivalence} de la relation $\mathscr{R}$.

Réciproquement, l'ensemble des classes d'équivalence associées à une relation d'équivalence sur $E$ forme une partition de $E$.

\paragraph{Ensemble quotient.} L'ensemble des classes d'équivalence est appelé \textbf{ensemble quotient} de $E$ par $\mathscr{R}$ et est noté $E|\mathscr{R}$. C'est ainsi que l'on construit les espaces fractionnels par exemple; les fractions sont les classes d'équivalences de la relation $(a, b) \mathscr{R} (c, d) \Leftrightarrow ad - bc = 0$.

\paragraph{Orientation d'un espace vectoriel.} On peut orienter un espace vectoriel de dimension finie en munissant l'ensemble de ses bases de la relation d'équivalence $\mathscr{B\,R\,B}' \Leftrightarrow \det_\mathscr{B}(\mathscr{B}') > 0$. Cette relation compte deux classes d'équivalences, donc deux orientations possibles d'un espace vectoriel.


\subsection{Groupes}

On appelle \textbf{groupe} toute couple $(G, \star)$ où $G$ est un ensemble \textit{non vide} et $\star$ est une \textit{loi de composition interne} vérifiant

\begin{enumerate}
	\item \textit{associativité}: $\forall (a, b, c) \in G^3, (a \star b) \star c = a \star (b \star c)$;
	\item existence d'un \textit{élément neutre}: $\exists 1_G \in G \st \forall a \in G, 1_G \star a = a \star 1_G = a$;
	\item existence du \textit{symétrique}: $\forall a \in G, \exists a^{-1} \in G \st a \star a^{-1} = a^{-1} \star a = 1_G$. 
\end{enumerate}

Un groupe est dit \textbf{abélien} lorsque la loi $\star$ est commutative.

\paragraph{Sous-groupe.} Soit $H \subset G$ non-vide, alors $(H, \star)$ est un \textbf{sous-groupe} de $(G, \star)$ lorsque $\forall (a, b) \in H^2, a \star b^{-1} \in H$. Un sous-groupe est un groupe.

\paragraph{Sous-groupe engendré.} Le sous-groupe engendré par $A$ noté $\mathrm{gr}(A)$ est l'intersection de tous les sous-groupes contenant $A$, donc le plus petit sous-groupe contenant $A$. En notation multiplicative,

$\mathrm{gr}(A) = \left\{ \prod_{j=1}^n g_j^{\varepsilon_j}, n \in \mathbb{N}^*,  (g_1, \ldots, g_n) \in A^n \text{ et } \forall j, \varepsilon_j = \pm 1 \right\}$.

\subsection{Morphismes de groupes}

Une application $f: G \to G'$, $(G, \star)$ et $(G', \star)$ étant deux groupes, est un \textbf{morphisme de groupes} lorsque $\forall (a, b) \in G^2, f(a \star b) = f(a) \star f(b)$. En cas de bijection, on parle d'\textbf{isomorphisme}.

Soient $H, H'$ deux sous-groupes de $G$ et $G'$ respectivement, alors $f(H)$ est un sous-groupe de $G'$ et $f^{-1}(H')$ est un sous-groupe de $G$. C'est en particulier vrai pour $\ker f$ et $\im f$.

\paragraph{Injectivité.} Un morphisme est injectif \textit{ssi} $\ker f = \{1_G\}$.


\subsection{Ordre}

\paragraph{Ordre d'un groupe.} L'\textbf{ordre} d'un groupe fini est son cardinal. L'ordre d'un sous-groupe $H$ de $G$ divise celui du groupe $G$, c'est le \textbf{théorème de Lagrange}. On peut montrer cela en considérant les ensembles $gH = \{gh, h\in H\}$. $G$ est alors l'union disjointe des classes d'équivalences pour la relation $g\mathscr{R}g' \Leftrightarrow g' \in gH$, d'où la conclusion.

\paragraph{Ordre d'un élément.} Soit $G$ un groupe. L'\textbf{ordre} d'un élément, s'il existe, est le plus petit entier $n$ non nul tel que $x^n = 1_G$.

\paragraph{Propriété.} Soit $g \in G$. Si $G$ est fini d'ordre $n$, alors
\begin{enumerate}
	\item $g$ est d'ordre fini $p$;
	\item $\mathrm{gr}(g)$ est d'ordre $p$;
	\item $g$ est générateur de $G$ \textit{ssi} $p=n$.
\end{enumerate}


\subsection{Anneaux}

On appelle \textbf{anneau} tout triplet $(A, +, \cdot)$ où $A$ est non vide, et où $+, \cdot$ sont des lois de composition internes vérifiant
\begin{enumerate}
	\item $(A, +)$ est un groupe \textit{abélien} d'élément neutre noté $0_A$;
	\item la loi $\cdot$ est \textit{associative}, possède un \textit{élément neutre} $1_A$ et est \textit{distributive} par rapport à la loi $+$.
\end{enumerate}

\paragraph{} Un anneau est \textbf{intègre} lorsque la loi $\cdot$ est \textit{commutative}, et lorsque qu'il est sans diviseur de zéro: $\forall (a, b) \in A^2, ab=0 \Rightarrow a=0 \text{ ou } b=0$. L'anneau $\mathbb{Z}/n\mathbb{Z}$ est intègre \textit{ssi} $n$ est premier.

\paragraph{Caractéristique.} Soit le morphisme d'anneaux $\varphi: \mathbb{Z} \to A, n \mapsto n 1_A$, et $n \in \mathbb{N}$ tel que $\ker \varphi = n\mathbb{Z}$. L'entier $n$ s'appelle la \textbf{caractéristique} de l'anneau $A$. Un anneau est soit de caractéristique nulle, soit de caractéristique égale à un entier \textit{premier}.

\paragraph{Corps.} Un \textbf{corps} est un anneau \textit{commutatif} dont tous les éléments non nuls admettent un symétrique pour la loi $\cdot$. Par ailleurs, l'ensembles des éléments inversibles d'un anneau est un groupe pour la loi $\cdot$.

\paragraph{Sous-anneau.} Une partie non vide $B$ de $A$ est un \textbf{sous-anneau} lorsque
\begin{enumerate}
	\item $(B, +)$ est un sous-groupe de $(A, +)$;
	\item $B$ est stable par multiplication;
	\item $1_A \in B$ (ne pas oublier!).
\end{enumerate}


\subsection{Morphismes d'anneaux}

Une application $f: A \to A'$, $A$ et $A'$ étant deux anneaux, est un \textbf{morphisme d'anneaux} lorsque
\begin{enumerate}
	\item $\forall (x, y) \in A^2, f(x+y) = f(x)+f(y)$;
	\item $\forall (x, y) \in A^2, f(x\cdot y) = f(x)\cdot f(y)$;
	\item $f(1_A) = 1_A'$.
\end{enumerate}

De même que pour les morphismes de groupes, les morphismes d'anneaux conservent la strucure d'anneau.


\subsection{Idéaux}

Soit $A$ un anneau commutatif, et $I$ une partie de $A$. Alors $I$ est un idéal lorsque
\begin{enumerate}
	\item $(I, +)$ est un sous-groupe de $(A, +)$;
	\item \textbf{stabilité par multiplication}: $AI = \{ab, (a, b) \in A\times I\}$ est inclus dans $I$.
\end{enumerate}

La somme, l'intersection de deux idéaux est un idéal.

\paragraph{Anneau principal.} Un idéal $(a) = aA$ engendré par un seul élément $a$ est dit \textbf{principal}. Un anneau \textit{intègre} dont les idéaux sont principaux est \textbf{principal}.


\subsection{Algèbre}

Une \textbf{algèbre} est un quadruplet $(A, +, \times, \cdot)$ où $A \neq \emptyset$, $+$ et $\times$ sont deux lois de composition \textit{internes} et $\cdot$ est une loi de composition \textit{externe}, et telles que
\begin{enumerate}
	\item $(A, +, \cdot)$ est un \kev;
	\item $(A, +, \times)$ est un anneau;
	\item $\forall \lambda \in \mathbb{K}, \forall (x, y) \in A^2, \lambda \cdot (x \times y) = (\lambda \cdot x) \times y = x \times (\lambda \cdot y)$.
\end{enumerate}

Une partie $B$ de $A$ est une \textbf{sous-algèbre} lorsque $(B, +, \cdot)$ est sev de $(A, +, \cdot)$, et $(B, +, \times)$ un sous-anneau de $(A, +, \times)$.

Enfin, une application $f$ est un \textbf{morphisme d'algèbre} lorsque c'est un morphisme d'espaces vectoriels et d'anneaux.


\subsection{Divisibilité}

Dans un anneau intègre, $a|b$ lorsque $\exists c \st b = ac$. On a l'équivalence $a|b \Leftrightarrow (b) \subset (a)$. 

Un élément est \textbf{inversible} lorsque c'est un diviseur de $1_A$. On a l'équivalence $(a) = (b) \Leftrightarrow \exists c \text{ inversible } \st b = ac$. On dit alors que $a$ et $b$ sont \textbf{associés}.

Un élément est \textbf{irréductible} lorsqu'il n'est pas inversible, et que ses seuls diviseurs lui sont associés ou sont les éléments inversibles de l'anneau.

Deux éléments sont \textbf{premiers entre eux} lorsque leurs seuls diviseurs communs sont les éléments inversibles de l'anneau.


\subsection{Notions de pgcd, ppcm}

Soit $A$ un anneau intègre. 

Un élément $d$ est \textit{un} \textbf{pgcd} de $a$ et $b$ lorsque c'est un diviseur commun à $a$ et $b$ et que tout autre diviseur commun à $a$ et $b$ divise $d$.

Un élément $m$ est \textit{un} \textbf{ppcm} de $a$ et $b$ lorsque c'est un multiple commun à $a$ et $b$ et que tout autre multiple commun à $a$ et $b$ est un multiple de $m$.

Deux pgcd ou deux ppcm sont \textit{associés}.

\subsection{Algèbre dans un anneau principal}

Si $A$ est principal, alors $\forall (a, b) \in A^2$, $a$ et $b$ admettent un pgcd et un ppcm. 

Les pgcd de $a$ et $b$ sont les générateurs de l'idéal $(a)+(b)$.

Les ppcm de $a$ et $b$ sont les générateurs de l'idéal $(a)\cap(b)$.

\paragraph{Relation de Bézout.} Soit $d$ un pgcd de $a$ et $b$, alors $\exists (u, v) \in A^2 \st ua + bv = d$.

\paragraph{Théorème de Bézout.} $a$ et $b$ sont premiers entre eux \textit{ssi} $\exists (u, v) \in A^2 \st ua + bv = 1$.

\paragraph{Théorème de Gauss.} Soient $a, b, c$ trois éléments. Si $a$ est premier avec $b$ et $a|bc$, alors $a|c$. 




\subsection{Propriétés algébriques de \texorpdfstring{$\mathbb{Z}$}{Z}}

Les sous-groupes et les idéaux de $\mathbb{Z}$ sont exactement de la forme $n\mathbb{Z}$. L'entier $n$ est unique.

\paragraph{Congruence.} Soit $n\in\mathbb{N}$ fixé. On définit la relation de \textbf{congruence modulo} sur $\mathbb{Z}$ par $x \equiv y [n] \Leftrightarrow y - x \in n\mathbb{Z}$. C'est une relation d'équivalence.

Deux entiers sont congrus modulo $n$ \textit{ssi} leurs divisions euclidiennes par $n$ donnent le même reste $r$. En outre, $\bar{x} = \bar{y} = \bar{r}$.

\paragraph{Ensemble quotient.} On note $\mathbb{Z}/n\mathbb{Z}$ le quotient de $\mathbb{Z}$ par cette relation et $\bar{x}^n = \mathrm{cl}(x)$. En outre, $\mathbb{Z}/n\mathbb{Z} = \{ \bar{0}, \bar{1}, \dots, \overline{n-1} \}$.

La relation de congruence est \textit{compatible avec l'addition et la multiplication}. On peut donc munir $\mathbb{Z}/n\mathbb{Z}$ d'une structure d'anneau. L'application $\Pi_n: x \mapsto \bar{x}$ est alors un morphisme d'anneaux surjectif.

\paragraph{Générateurs de $\mathbb{Z}/n\mathbb{Z}$.} L'élément $\bar{k}$ est générateur de $\mathbb{Z}/n\mathbb{Z}$ \textit{ssi} $k$ et $n$ sont \textit{premiers entre eux}.

\paragraph{Éléments inversibles.} On note $\mathscr{U}(\mathbb{Z}/n\mathbb{Z})$ l'ensemble des éléments inversibles de $\mathbb{Z}/n\mathbb{Z}$. L'élément $\bar{k} \in \mathbb{Z}/n\mathbb{Z}$ est inversible \textit{ssi} $k$ et $n$ sont premiers entre eux.

\paragraph{Relation entre pgcd et ppcm.} Dans $\mathbb{Z}$ on peut parler du pgcd et du ppcm. On a la relation $ab = (a\wedge b)(a\vee b)$.


\subsection{Fonction indicatrice d'Euler} 

La \textbf{fonction indicatrice d'Euler} est définie sur $\mathbb{N}^*$ par

$\begin{cases}
	\varphi(1) = 1 \\ 
	\forall n \geq 2, \varphi(n) = \card \mathscr{U}(\mathbb{Z}/n\mathbb{Z}).
\end{cases}$

Ainsi, $\varphi(n)$ donne le nombre d'entiers de $\intr{1, n-1}$ premiers avec $n$.

\paragraph{Calcul.} Soit $n \in \mathbb{N}, n\geq2$, et soit $n = \prod_{i=1}^k p_i^{\alpha_i}$ sa décomposition en facteurs premiers. 

Alors $\varphi(n) = \prod\limits_{i=1}^k p_i^{\alpha_i - 1} (p_i - 1)$.

\paragraph{Démonstration.} Traîter le cas $n = p^\alpha$ avec $p$ premier et montrer que $\varphi(n) = p^\alpha - p^{\alpha-1}$. Traîter ensuite le cas général à l'aide du théorème chinois.

\paragraph{Théorème d'Euler.} Soit $n \geq 2$ et $k$ premier avec $n$. Alors $k^{\varphi(n)} \equiv 1 [n]$.

\paragraph{Petit théorème de Fermat.} C'est un corollaire du théorème d'Euler. Soit $k \in \mathbb{N}^*$ et $p$ premier, non diviseur de $k$. Alors $k^p \equiv k [p]$.

\subsection{Groupes monogènes}

Un groupe engendré par un seul élément est appelé \textbf{groupe monogène}. Si de plus il est fini, c'est un \textbf{groupe cyclique}.

\paragraph{Classification.} Soit $G$ un groupe, et $g \in G$. Considérons le morphisme $\varphi: \mathbb{Z} \to G, k \mapsto g^k$. Notons $\ker \varphi = n\mathbb{Z}$. Alors
\begin{enumerate}
	\item si $n=0$, $\varphi$ induit un isomorphisme entre $\mathbb{Z}$ et $\im \varphi = \mathrm{gr}(g)$;
	\item si $n>0$, $\im \varphi = \mathrm{gr}(g)$ est isomorphe à $\mathbb{Z}/n\mathbb{Z}$.
\end{enumerate}

En d'autres termes, les groupes monogènes sont soit isomorphes à $\mathbb{Z}$, soit cycliques et isomorphes à $\mathbb{Z}/n\mathbb{Z}$.

\paragraph{Démonstration.} Pour le deuxième point, montrer l'existence d'un morphisme $\psi$ tel que $\varphi = \psi \circ \Pi_n$.


\subsection{Théorème chinois}

Soient $n, p$ deux entiers naturels premiers entre eux. Alors $\mathbb{Z}/n\mathbb{Z} \times \mathbb{Z}/p\mathbb{Z} \approx \mathbb{Z}/np\mathbb{Z}$. Plus précisemment, il y a isomorphisme d'anneau.

\paragraph{Démonstration.} Soit $\varphi : x \mapsto (\bar{x}^n, \bar{x}^p)$. Montrer qu'il existe un unique morphisme d'anneaux $\psi$ tel que $\varphi = \psi \circ \Pi_{np}$, et que c'est un isomorphisme.