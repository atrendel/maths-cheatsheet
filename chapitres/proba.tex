\section{Probabilités discrètes}

\subsection{Tribus, probabilité}

\paragraph{Tribu.} Soit $\Omega$ un ensemble, $\mathscr{T} \subset \parties{\Omega}$ est une \textbf{tribu} lorsque
\begin{enumerate}
	\item $\Omega \in \mathscr{T}$;
	\item $\forall A \in \mathscr{T}, \Omega \backslash A \in \mathscr{T}$;
	\item \textbf{stabilité par union dénombrable:} $\forall (A_n)_{n\geq0} \in \mathscr{T}^\mathbb{N}, \bigcup\limits_{n\geq0} A_n \in \mathscr{T}$.
\end{enumerate}

Par exemple, $\parties{\Omega}$ est une tribu. Cette définition implique notamment que $\mathscr{T}$ est stable par \textit{union} et \textit{intersection} au plus dénombrable.

\paragraph{Vocabulaire.} Un élément $A$ de $\mathscr{T}$ est un \textit{événement}. Deux événements $A$ et $B$ sont \textit{incompatibles} lorsque $A \cup B = \emptyset$. Un \textit{système complet d'événements} est une partition de l'\textit{univers} $\Omega$. Un événement $A$ est \textit{presque sûr} lorsque sa probabilité vaut 1.

\paragraph{Probabilité.} On appelle \textbf{probabilité} sur $(\Omega, \mathscr{T})$ toute application $P:\mathscr{T} \to [0,1]$ telle que
\begin{enumerate}
	\item $P(\Omega) = 1$;
	\item \textbf{$\sigma$-additivité:} pour toute suite $(A_n)_{n\geq0}$ d'événements \textit{incompatibles} deux à deux, la famille $(P(A_n))_{n\geq0}$ est sommable et vérifie
	
	$P(\bigcup\limits_{n\geq0} A_n) = \sum\limits_{n=0}^{+\infty} P(A_n)$.
\end{enumerate}
Le triplet $(\Omega, \mathscr{T}, P)$ est un \textbf{espace probabilisé}.


\subsection{Propriétés des probabilités}

\paragraph{Quelques propriétés.}
\begin{enumerate}
	\item la $\sigma$-additivité s'étend naturellement au cas au plus dénombrable;
	\item $P(\bar{A}) = 1 - P(A)$;
	\item $P(A\cup B) = P(A) + P(B) - P(A\cap B)$ et $P(A\cup B) \leq P(A) + P(B)$;
	\item si $A \subset B$, $P(B\backslash A) = P(B) - P(A)$ et $P(A) \leq P(B)$.
\end{enumerate}

\paragraph{Continuité croissante, décroissante.} Soit $(A_n)_{n\geq0}$ une suite d'événements. 

Si la suite est \textit{croissante}, $P(A_n) \to P(\bigcup_{n=0}^{+\infty} A_n)$. 

Si la suite est \textit{décroissante}, $P(A_n) \to P(\bigcap_{n=0}^{+\infty} A_n)$.

\paragraph{Inégalité de Boole.} Soit $(A_n)_{n\geq0}$ une suite d'événements, $(P(A_n))_{n\geq0}$ sommable.

Alors $P(\bigcup_{n=0}^{+\infty} A_n) \leq \sum_{n=0}^{+\infty} P(A_n)$.



\subsection{Probabilité conditionnelle} 

On appelle \textbf{probabilité conditionnelle} de $A$ sachant $B$ ($B$ non négligeable) la quantité $P(A|B) = P_B(A) = \frac{P(A\cap B)}{P(B)}$. La fonction $P_B$ est une probabilité.


\subsection{Indépendance}

Deux événements $A$ et $B$ sont dits \textbf{indépendants} lorsque $P(A\cap B) = P(A)P(B)$, ou de manière équivalente en termes de probabilité conditionnelle, lorsque $P(A) = P(A|B)$.

On dits que les événements $(A_i)_{i\in I}$ sont \textbf{mutuellement indépendants} lorsque $\forall J \text{ fini}, J \subset I, P(\bigcap\limits_{j\in J} A_j) = \prod\limits_{j\in J} P(A_j)$. 

\textbf{Attention,} cela implique l'indépendance des événements deux à deux, mais la réciproque n'est pas vraie.


\subsection{Formules usuelles}

\paragraph{Formule du crible.} Soient $A_1, \ldots, A_n$ $n$ événements.
\begin{equation*}
	P(\bigcup_{k=1}^n A_k) = \sum_{k=1}^n (-1)^{k-1} \sum_{\mathclap{1 \leq j_1 < \ldots < j_k \leq n}} P(A_{j_1} \cap \ldots \cap A_{j_k}).
\end{equation*}

\paragraph{Démonstration.} On peut la démontrer à l'aide des fonctions indicatrices.

\paragraph{Formule des probabilités composées.} Soient $A_1, \ldots, A_n$ $n$ événements tels que $P(A_1\cap\ldots\cap A_{n-1}) \neq 0$.
\begin{equation*}
	P(A_1\cap\ldots\cap A_n) = P(A_1) P(A_2|A_1) P(A_3|A_1\cap A_2) \ldots P(A_n | A_1\cap\ldots\cap A_{n-1}).
\end{equation*}

\paragraph{Formule des probabilités totales.} Soit $(E_i)_{i\in I}$ un système complet d'événements non négligeables, où $I$ est au plus dénombrable.
\begin{equation*}
	\forall A \in \mathscr{T}, P(A) = \sum_{i \in I} P(A|E_i) P(E_i).
\end{equation*}

\paragraph{Formule de Bayes.} Soient $A, B$ deux événements non négligeables.
\begin{equation*}
	P(A|B) = \frac{P(B|A) P(A)}{P(B)}.
\end{equation*}


\subsection{Variables aléatoires discrètes}

On appelle \textbf{variable aléatoire discrète} toute application $X:\Omega \to E$ telle que
\begin{enumerate}
	\item $X(\Omega)$ est au plus dénombrable;
	\item $\forall x \in X(\Omega), X^{-1}(\{x\}) \in \mathscr{T}$.
\end{enumerate}

On peut vérifier que $\forall A \subset E, X^{-1}(A) \in \mathscr{T}$.

\paragraph{Notations.} Pour $A \subset E$, on note $(X \in A)$ l'événement $X^{-1}(A)$, et si $A = \{x\}$, on le note simplement $(X = x)$. Pour une variable aléatoire discrète \textit{réelle}, on note $(X \leq a)$ l'événement $X^{-1}(]-\infty, a])$.

\paragraph{Loi d'une variable.} L'application $P_X: \begin{aligned} \parties{X(\Omega)} \to \mathbb{R} \\ A \mapsto P(X \in A) \end{aligned}$ est une probabilité sur $(X(\Omega), \parties{X(\Omega)})$, et s'appelle la \textbf{loi de $X$}.


\subsection{Association de variables aléatoires}

Si $X$ et $Y$ sont deux variables aléatoires discrètes, $(X, Y)$ en est une. Si $X$ et $Y$ sont réelles, $\lambda X + \mu Y$ est encore une variable réelle. Enfin, pour toute fonction définie sur $X(\Omega)$, $f \circ X$ est une variable aléatoire discrète. 

\paragraph{Loi conjointe, loi marginale.} La \textbf{loi conjointe} de deux variables $X$ et $Y$ est la loi de la variable $(X, Y)$, c'est-à-dire la loi définie sur $P((X, Y) = (x, y)) = P((X=x)\cap(Y=y))$. Elle détermine les \textbf{lois marginales} de $X$ et $Y$ (les lois de $X$ et de $Y$), mais la réciproque n'est pas (toujours) vraie!


\subsection{Espérance}

Une variable aléatoire discrète $X$ admet une \textbf{espérance} lorsque la famille $(x P(X=x))_{x \in X(\Omega)}$ est sommable, et elle est alors donnée par 

$E(X) = \sum\limits_{x\in X(\Omega)} x P(X=x)$.

Celle-ci est toujours définie pour une variable à valeurs dans $\mathbb{R}_+$.

On dit d'une variable dont l'espérance est nulle qu'elle est \textbf{centrée}.

\paragraph{Fonction indicatrice.} Soit $A$ un événement, la \textbf{fonction indicatrice} associée, notée $1_A$, est définie pour $\omega\in\Omega$ et vaut 1 si $\omega\in A$, 0 sinon. De plus, $E(1_A) = P(A)$.

% \paragraph{} On peut écrire $X$ sous la forme $X = \sum_{i \in I} a_i 1_{A_i}$, ou $I$ est au plus dénombrable, $X(\Omega) = \{a_i, i \in I \}$ (les $a_i$ ne sont pas nécessairement distincts deux à deux), et $(A_i)_{i\in I}$ est une partition de $\Omega$.
% 
% En outre, $X$ admet une espérance \textit{ssi} $(a_i P(A_i))$ est sommable, donnée par $E(X) = \sum\limits_{i \in I} a_i P(A_i)$.
% 
% \paragraph{Linéarité.} À l'aide la décomposition précédente, on peut montrer que l'espérance est \textit{linéaire}. Avec les notations précédentes, pour deux variables $X$ et $Y$, poser $Z = X + Y$, et établir la décomposition suivante avec $C_{ij} = A_i\cap B_j$:
%  $Z = \sum\limits_{(i,j) \in I\times J} (a_i+b_j) 1_{C_{ij}}$. Il reste à monter la sommabilité.

\paragraph{Linéarité.} L'espérance est linéaire. On peut le montrer à l'aide de la formule des probabilités totales.

\paragraph{Croissance.} On peut montrer que si $X \geq 0$ et admet une espérance, alors $E(X) \geq 0$. La linéarité permet donc d'aboutir à la croissance.

\paragraph{Autres propriétés.} \begin{enumerate}
	\item si $X$ est égale à une constante $C$ (presque sûrement ou non), $X$ admet une espérance et $E(X) = C$;
	\item si $X \geq 0$ (presque sûrement ou non) et $E(X) = 0$, alors $X = 0$ presque sûrement;
	\item $X$ admet une espérance \textit{ssi} $|X|$ admet une espérance.
\end{enumerate}

\paragraph{Théorème de transfert.} Soit $X$ une variable aléatoire et $f: X(\Omega) \to \mathbb{R}$. Alors $f \circ X$ admet une espérance \textit{ssi} la famille $(f(x)P(X=x))_{x\in X(\Omega)}$ est sommable, et $E(f\circ X) = \sum\limits_{x\in X(\Omega)} f(x) P(X=x)$.


\subsection{Indépendance de variables aléatoires}

Deux variables aléatoires $X$ et $Y$ sont \textbf{indépendantes} lorsque $\forall (x, y) \in X(\Omega) \times Y(\Omega), P((X=x)\cap(Y=y)) = P(X=x) P(Y=y)$, c'est-à-dire lorsque les événements $(X=x)$ et $(Y=y)$ sont indépendants. Si $X$ et $Y$ sont indépendantes, les lois marginales déterminent la loi conjointe de $X$ et $Y$.

\paragraph{} Les variables aléatoires $X_1, \ldots, X_n$ sont dites \textbf{mutuellement indépendantes} lorsque $\forall (x_1, \ldots, x_n) \in \prod_{j=1}^n X_j(\Omega), P(X_1=x_1 \cap\ldots\cap X_n = x_n) = \prod_{j=1}^n P(X_j = x_j)$. Pour $n=2$, on retrouve l'indépendance.

\paragraph{Lemme des coalitions.} Soient $X_1, \ldots, X_n$ $n$ variables aléatoires \textit{mutuellement indépendantes}. Alors pour tout $k \in \intr{1, n-1}$, et toutes fonctions $f: X_1(\Omega) \times\ldots\times X_k(\Omega) \to F$ et $g: X_{k+1}(\Omega) \times\ldots\times X_n(\Omega) \to G$, les variables aléatoires $f(X_1, \ldots, X_k)$ et $g(X_{k+1}, \ldots, X_n)$ sont indépendantes.

\paragraph{Espérance.} Si $X$ et $Y$ sont indépendantes et admettent une espérance, $XY$ admet une espérance et $E(XY) = E(X)E(Y)$.


\subsection{Moments}

Une variable aléatoire $X$ admet un \textbf{moment d'ordre $r$} lorsque $X^r$ admet une espérance et celui ci vaut $E(X^r)$ (que l'on calcule simplement par théorème de transfert). Si une variable admet un moment d'ordre $r$, elle admet un moment pour tout ordre inférieur.

On dit que $X$ admet un \textbf{moment centré d'ordre $r$} lorsque $X - E(X)$ admet un moment d'ordre $r$, il vaut alors $E((X-E(X))^r)$. En outre, $X$ admet un moment d'ordre $r$ \textit{ssi} elle admet un moment centré d'ordre $r$.

\paragraph{Variance.} La \textbf{variance} de $X$ est son moment centré d'ordre 2 de valeur $V(X) = E((X-E(X))^2)$, sous réserve d'existence. L'\textbf{écart-type} de $X$ noté $\sigma(X)$ est la racine carrée de sa variance; lorsque celui-ci vaut 1, la variable est dite \textbf{réduite}.

On a l'équivalence $V(X) = 0 \Leftrightarrow X$ est constante presque sûrement. En outre, $V(\lambda X) = \lambda^2 V(X)$ et $V(X+\lambda) = V(X)$.


\subsection{Covariance} 

Pour deux variables $X$ et $Y$ telles que $XY$ admette une espérance, on définit la \textbf{covariance} par $\mathrm{cov}(X, Y) = E( (X-E(X)) (Y-E(Y)) )$. 

On retrouve une formule de König-Huygens: $\mathrm{cov}(X, Y) = E(XY) - E(X)E(Y)$. Ainsi, en cas d'indépendance, $\mathrm{cov}(X, Y) = 0$.

\paragraph{Coefficient de corrélation.} Il est défini par $p(X, Y) = \frac{\mathrm{cov}(X, Y)}{\sigma(X) \sigma(Y)}$. On a $|p(X, Y)| = 1$ \textit{ssi} les variables centrées-réduites associées à $X$ et $Y$ sont proportionnelles presque sûrement, le coefficient étant $p(X, Y)$.


\subsection{Formules liées aux moments}

\paragraph{Formule de König-Huygens.} $V(X) = E(X^2) - E(X)^2$.

\paragraph{Inégalité de Cauchy-Schwarz.} $E(XY)^2 \leq E(X^2) E(Y^2)$, égalité \textit{ssi} la famille $(X, Y)$ est liée presque sûrement.

\paragraph{Inégalité de Minkowski.} $\sqrt{E((X+Y)^2)} \leq \sqrt{E(X^2)} + \sqrt{E(Y^2)}$.

\paragraph{Inégalité de Markov.} $\forall \varepsilon > 0, P(X \geq \varepsilon) \leq \frac{E(X)}{\varepsilon}$.

\paragraph{Inégalité de Bienaymé-Tchebychev.} Pour $\varepsilon > 0$, on a:

$P(|X - E(X)| \geq \varepsilon) \leq \frac{V(X)}{\varepsilon^2}$.


\subsection{Loi faible des grands nombres}

Soit $(X_n)$ une suite de variables \textit{indépendantes deux à deux}, de même loi, de même espérance $m$ et admettant une variance.
Alors en notant $S_n = \sum_{k=1}^n X_k$, on a 

$\forall \varepsilon > 0, P(| \frac{1}{n} S_n - m | \geq \varepsilon) \to 0$.   


\subsection{Fonctions génératrices}

Pour $X$ une variable à valeurs dans $\mathbb{N}$, on définit la \textbf{fonction génératrice} de $X$ par la somme de $\sum P(X=n)t^n$ et on la note $G_X$. Par théorème de transfert, en tout point de convergence, $G_X(t) = E(t^X)$.

On peut montrer que $G_X$ est définie au moins sur $[-1, 1]$ et $\cont^\infty$ sur $]-1,1[$. Elle est monotone sur $[0, 1[$, croissante et convexe sur $[0, 1]$.

\paragraph{Indépendance.} Pour $X_1, \ldots, X_n$ mutuellement indépendantes, en notant $X = \prod X_k$, on a $G_X = \prod G_{X_k}$.

\paragraph{Calcul d'espérance, de variance.} \begin{enumerate}
	\item $X$ admet une espérance \textit{ssi} $G_X$ est dérivable en 1, et dans ce cas $E(X) = G_X'(1)$;
	\item $X$ admet une variance \textit{ssi} $G_X$ est deux fois dérivable en 1, et dans ce cas $V(X) = G_X''(1) + G_X'(1) - (G_X'(1))^2$.
\end{enumerate}


\subsection{Lois discrètes classiques}

\paragraph{Loi uniforme.} Soit $X$ une variable aléatoire à valeurs dans un ensemble $E = \{ x_1, \ldots, x_n \}$ fini. 

On note $X \sim \mathcal{U}(E)$ et $P(X = x_k) = \frac{1}{n}$.

\paragraph{Loi de Bernoulli.} Avec $X(\Omega) = \{0,1\}, p \in ]0, 1[$.

On note $X \sim \mathscr{B}(p)$ et $P(X = 1) = p$.

\paragraph{Loi binomiale.} Avec $X(\Omega) = \intr{0, n}, n \in \mathbb{N}^*, p \in ]0, 1[$.

On note $X \sim \mathscr{B}(n, p)$ et $P(X = k) = \binom{n}{k} p^k (1-p)^{n-k}$. Elle donne le nombre de succès.

\paragraph{Loi géométrique.} Avec $X(\Omega) \subset \mathbb{N}^*, p \in ]0, 1[$.

On note $X \sim \mathscr{G}(p)$ et $P(X = k) = (1-p)^{k-1} p$. Elle donne le rang du premier succès.

En outre, la loi d'une variable aléatoire est \textit{sans mémoire} \textit{ssi} elle est géométrique. Une loi est dite \textbf{sans mémoire} lorsque $X(\Omega) = \mathbb{N}^*$ et $\forall (k, l) \in {\mathbb{N}^*}^2, P(X > k+l | X > l) = P(X > k)$.

\paragraph{Loi de Poisson.} Avec $X(\Omega) = \mathbb{N}, \lambda \in \mathbb{R}^*_+$.

On note $X \sim \mathscr{P}(\lambda)$ et $P(X = k) = \mathrm{e}^{-\lambda} \frac{\lambda^k}{k!}$.

\vspace{1cm}

{\tabulinesep=1.5mm
\begin{tabu}{|c|c|c|c|}
	\hline
	Loi de $X$ & $E(X)$ & $V(X)$ & $G_X(t)$ \\
	\hline
	
	$\mathcal{U}(\intr{1, n})$ 
	& $\frac{n+1}{2}$ 
	& $\frac{n^2-1}{12}$ 
	& $\frac{1}{n} \frac{t(1-t^n)}{1-t}$ pour $t \neq 1$, sinon 1\\
	
	$\mathscr{B}(p)$ 
	& $p$ 
	& $p(1-p)$ 
	& $1 - p + pt$ \\
	
	$\mathscr{B}(n, p)$ 
	& $np$ 
	& $np(1-p)$ 
	& $(1 - p + pt)^n$ \\
	
	$\mathscr{G}(p)$ 
	& $\frac{1}{p}$ 
	& $\frac{1-p}{p^2}$ 
	& $\frac{pt}{1-(1-p)t}$ pour $t \in ]-\frac{1}{1-p}, \frac{1}{1-p}[$ \\
	
	$\mathscr{P}(\lambda)$ 
	& $\lambda$ 
	& $\lambda$ 
	& $\mathrm{e}^{\lambda (t-1)}$ \\ 
	
	\hline
\end{tabu}}