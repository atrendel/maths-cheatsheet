\section{Réduction des endomorphismes}


\subsection{Polynômes d'endomorphismes}

Soit $E$ un \kev{}, $u \in \lin(E), P = \sum\limits_{k=0}^n a_k X^k \in \mathbb{K}[X]$. On définit $P(u) \in \lin(E)$ par $P(u) = \sum\limits_{k=0}^n a_k u^k \in \mathbb{K}[X]$. 

Le calcul de $P(u)$ se fait de manière naturelle. En effet, l'application $\chi$ définie par $\chi: \begin{aligned}
	\mathbb{K}[X] &\to \lin(E) \\ P &\mapsto P(u).
\end{aligned}$ est un morphisme d'algèbre.


\subsection{Sous-espaces stables}

Un sev $F$ du \kev{} $E$ est stable par $u \in \lin(E)$ lorsque $u(F) \subset F$.

Si $(u, v) \in \lin{}(E)^2$ commutent, $\ker u$ et $\im u$ sont stables par $v$.

De plus, si $u$ et $v$ commutent, $\forall (P, Q) \in \mathbb{K}[X]^2, P(u)$ et $Q(v)$ commutent.

En particulier, $u$ commute avec lui-même donc $\ker P(u)$ et $\im P(u)$ sont stables par $u$.


\subsection{Polynômes annulateurs, polynôme minimal}

Soit $E$ un \kev{} et $u \in \lin(E)$, alors $\ker \chi$ est un idéal de $\mathbb{K}[X]$. C'est l'idéal annulateur de $u$, ses éléments sont les \textbf{polynômes annulateurs} de $u$.

L'unique polynôme unitaire qui engendre $\ker \chi$ est le \textbf{polynôme minimal} $\Pi_u$.

\paragraph{Propriété.} Soit $F$ un sev de $E$ stable par $u$, et $v$ l'endomorphisme induit par $u$ sur $F$. Alors $v$ admet un polynôme minimal et $\Pi_v | \Pi_u$.


\subsection{Théorème des noyaux}

Soient $E$ un \kev{}, $u \in \lin(E), (S_1, \ldots, S_n) \in \mathbb{K}[X]^n$ deux à deux premiers entre eux. Notons $S = \prod_{j=1}^n S_j$.

Alors $\ker S(u) = \bigoplus\limits_{j=1}^n \ker S_j(u)$.

\paragraph{Corollaire.} Si $S$ annule $u$, est scindé et se met sous la forme $ \alpha \prod_{j=1}^n (X - \lambda_j)^{\alpha_j}$, les $\lambda_j$ étant distincts deux à deux,
alors $E = \bigoplus\limits_{j=1}^n \ker ( u - \lambda_j \id )^{\alpha_j} $. 


\subsection{Vecteurs, valeurs, espaces propres}

Soient $E$ un \kev{}, $u \in \lin(E)$. On appelle \textbf{vecteur propre} de $u$ tout vecteur $x$ non nul tel que $\exists \lambda \st u(x) = \lambda x$. $\lambda$ est la \textbf{valeur propre} associée à $x$. On appelle \textbf{spectre} l'ensemble $\Sp (u)$ des valeurs propres de $u$. On définit cela de la même manière pour les matrices.

\paragraph{Propriété.} Une valeur propre est racine d'un polynôme annulateur.

\paragraph{} On appelle \textbf{sous-espace propre} associé à la valeur propre $\lambda$ l'espace $E_\lambda(u) = \ker (u - \lambda \id_E)$.


\subsection{Polynôme caractéristique}
\label{sec:polycar}

Soit $A \in \matsp{n}$, on appelle \textbf{polynôme caractéristique} de $A$ le polynôme $\chi_A = \det(X I_n - A)$. Le polynôme caractéristique de $u \in \lin(E)$ est défini par $\chi_u = \chi_A$ où $A = \mat_\mathscr{B}(u)$ dans une base $\mathscr{B}$ donnée.

\paragraph{Propriété.} $\Sp(A)$ est égal à l'ensemble des racines du polynôme caractéristique.

\paragraph{Forme de $\chi_u$.} Il est unitaire de degré $n = \dim E$.

$\chi_u = X^n - \tr u \, X^{n-1} + \ldots + (-1)^n \det u$.
 
Ainsi, si $\chi_u$ est scindé, en notant $\lambda_1, \ldots, \lambda_n$ les valeurs propres non nécessairement distinctes deux à deux, il vient 

$\tr u = \sum\limits_{j=0}^n \lambda_j$ et $\det u = \prod\limits_{j=0}^n \lambda_j$.
 
\paragraph{Propriété.} Soit $F$ un sev de $E$ stable par $u$, et $v$ l'endomorphisme induit par $u$ sur $F$. Alors $v$ admet un polynôme caractéristique et $\chi_v | \chi_u$.

\paragraph{Matrices semblables.} Deux matrices semblables ont le même polynôme caractéristique.

\paragraph{Application à la densité.} On peut utiliser le polynôme caractéristique pour montrer la densité de $\mathrm{GL}_n(\mathbb{K})$ dans $\matsp{n}$. Soit $A \in \matsp{n}$, et $A_k = A - \frac{1}{k} I_n$. Alors $\det A_k = \det (A - \frac{1}{k} I_n) = (-1)^n \det (\frac{1}{k} I_n - A) = (-1)^n \chi_A(\frac{1}{k})$. Les racines de $\chi_A$ étant isolées, la suite $(\det A_k)$ est non nulle à partir d'un certain rang, on a donc construit une suite de matrices inversible tendant vers A.


\subsection{Multiplicité}
 
Soient $E$ un \kev{}, $u \in \lin(E), \lambda \in \Sp(u)$. On appelle
\begin{enumerate}
	\item \textbf{multiplicité géométrique} de $\lambda$ l'entier $\dim E_\lambda(u)$;
	\item \textbf{multiplicité algébrique} de $\lambda$ sa multiplicité comme racine du polynôme caractéristique.
\end{enumerate}

\paragraph{Théorème.} La multiplicité géométrique est inférieure ou égale à la multiplicité algébrique.


\subsection{Théorème de Hamilton-Cayley}

Soient $E$ un \kev{} de dimension finie, $u \in \lin(E)$. Alors $\Pi_u | \chi_u$, c'est-à-dire que $\chi_u$ est un polynôme annulateur.

\paragraph{Corollaire.} Ils ont de plus les mêmes racines et les mêmes facteurs irréductibles dans $\mathbb{K}$.


\subsection{Diagonalisation}

Soit $u \in \lin(E)$, $u$ est \textbf{diagonalisable} lorsque $E = \bigoplus\limits_{\lambda \in \Sp(u)} E_\lambda(u)$. 

Ceci est équivalent au fait qu'il existe une base où la matrice de $u$ est diagonale, ou encore qu'il existe une base de $E$ formée de vecteur propres, etc.

\paragraph{Condition suffisante.} $u$ admet $n =\dim E$ valeurs propres distinctes.

\paragraph{Quelques théorèmes.} 
\begin{enumerate}
	\item $u$ est diagonalisable \textit{ssi} $\dim E = \sum\limits_{\lambda \in \Sp(u)} \dim E_\lambda(u)$;
	\item $u$ est diagonalisable \textit{ssi} $\chi_u$ est scindé et les multiplicités algébriques et géométriques de chaque valeur propre sont égales;
	\item $u$ est diagonalisable \textit{ssi} $\Pi_u$ est scindé à racines simples.
\end{enumerate}

\paragraph{Diagonalisation simultanée.}

Si $(u, v) \in \lin(E)^2$ commutent et sont diagonalisables, $u$ et $v$ sont \textit{simultanément} diagonalisable. 

\paragraph{Démonstration.} Il s'agit de diagonaliser les endomorphismes induits par $v$ sur chacun des sous-espaces propres de $u$ (c'est bien licite, voir \ref{sec:polycar}). La base obtenue est une base de vecteur propres pour $u$ et pour $v$.


\subsection{Trigonalisation}

Un endomorphisme $u$ est \textbf{trigonalisable} lorsqu'il existe une base où sa matrice est triangulaire.

\paragraph{Quelques théorèmes.} 
\begin{enumerate}
	\item $u$ est trigonalisable \textit{ssi} $\chi_u$ est scindé;
	\item $u$ est trigonalisable \textit{ssi} $\Pi_u$ est scindé.
\end{enumerate}

\paragraph{Démonstration.} Pour le premier point, la réciproque se fait par récurrence sur la dimension de l'espace. On se ramène à un espace de dimension inférieure en prenant un vecteur propre (il en existe par hypothèse). Un changement de base fait apparaître une matrice intermédiaire où appliquer l'hypothèse de récurrence. Pour le deuxième point, si $\Pi_u$ ou $\chi_u$ est scindé, comme ils ont les mêmes facteurs irréductibles, l'autre est scindé.