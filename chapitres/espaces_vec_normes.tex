\section{Espaces vectoriels normés}


\subsection{Norme}

Une application d'un \kev{} $E$ dans $\mathbb{R}_+$ est une \textbf{norme} si
\begin{enumerate}
	\item $\forall \lambda \in \mathbb{K}, \forall x \in E, \norm{\lambda x} = |\lambda| \norm{x}$ (homogénéité);
	\item $\forall x \in E, \norm{x} = 0 \Leftrightarrow x = 0$ (séparation);
	\item $\forall (x, y) \in E^2, \norm{x+y} \leq \norm{x} + \norm{y}$ (inégalité triangulaire).
\end{enumerate}

\paragraph{Norme d'algèbre.} Une norme $N$ est une \textbf{norme d'algèbre} sur une $\mathbb{K}$-algèbre $A$ lorsque
\begin{enumerate}
	\item $\forall (x, y) \in A^2, N(xy) \leq N(x)N(y)$ ($N$ est sous multiplicative);
	\item $N(1_A) = 1$.
\end{enumerate}

\paragraph{Normes équivalentes.} $N$ et $N'$ sont \textbf{équivalentes} lorsque 

$\exists (\alpha, \beta) \in (\mathbb{R}_+^*)^2 \st \forall x, \alpha N(x) \leq N'(x) \leq \beta N(x)$.

C'est une relation d'équivalence.

\paragraph{Lien avec les suites convergentes.} $N$ et $N'$ équivalentes \textit{ssi} l'ensemble des suites qui tendent vers 0 pour $N$ est égal à l'ensemble des suites qui tendent vers 0 pour $N'$.

\paragraph{Démonstration.} Montrer que toute suite qui converge pour $N$ converge pour $N'$ \textit{ssi} $\exists \alpha > 0 \st N' \leq \alpha N$. Réciproque par l'absurde.

\subsection{Normes classiques}

\paragraph{Norme euclidienne.} L'application $x \mapsto \sqrt{\scp{x}{x}}$ est la \textbf{norme euclidienne}.

\paragraph{Sur $\mathbb{K}^n, \matsp{n, p}, \mathbb{K}[X]$.} On définit trois normes:

\begin{enumerate}
	\item convergence en moyenne: $N_1: x \mapsto \sum\limits |x_j|$;
	\item convergence quadratique: $N_2: x \mapsto \sqrt{ \sum\limits |x_j|^2 }$;
	\item convergence uniforme: $N_\infty: x \mapsto \max |x_j|$.
\end{enumerate}

En outre, $\forall x \in \mathbb{K}^n, \norm{x}_\infty \leq \norm{x}_2 \leq \norm{x}_1 \leq \sqrt{n} \norm{x}_2 \leq n \norm{x}_\infty$.

\paragraph{Sur les espaces fonctionnels.}

Avec $\mathscr{B}(X, E)$ l'espace des fonctions bornées d'un ensemble $X$ dans un espace normé $(E, N)$: 

$ N_\infty: f \mapsto \sup\limits_{x \in X} N(f(x))$.

Idem sur l'espaces des suites de $E^\mathbb{N}$ bornées.

\paragraph{Sur $\mathscr{C}([a, b], \mathbb{K})$.} On définit trois normes.

\begin{enumerate}
	\item $N_1: f \mapsto \int_a^b |f(t)| \ud t$;
	\item $N_2: f \mapsto \sqrt{ \int_a^b |f(t)|^2 \ud t }$;
	\item $N_\infty: f \mapsto \sup\limits_{x \in [a, b]} |f(x)|$.
\end{enumerate}

En outre, $N_1(f) \leq \sqrt{b-a} N_2(f) \leq (b-a) N_\infty (f)$.


\subsection{Valeur d'adhérence d'une suite}

Soit $E$ un \kevn{}, $u \in E^\mathbb{N}$ et $a \in E$.
Si il existe une suite extraite de $u$ convergeant vers $a$, $a$ est une valeur d'adhérence.

\paragraph{Caractérisation.} $a$ est une valeur d'adhérence \textit{ssi} $ \forall \varepsilon > 0, \forall N \in \mathbb{N}, \exists n \geq N \st \norm{u_n - a} \leq \varepsilon$. 

\paragraph{Propriété.} Une suite convergente admet une unique valeur d'adhérence. La réciproque est vraie dans un compact.


\subsection{Topologie}

\paragraph{Ouvert, fermés.} Soit $A$ une partie d'un \kevn{} $E$. $\bo(x, r)$ désigne la boule ouverte centrée en $x$ de rayon $r$.

On dit que $A$ est un \textbf{ouvert} lorsque $ \forall  a \in A, \exists r > 0 \st \bo(a, r) \subset A$.

On dit que $A$ est un \textbf{fermé} lorsque $E \setminus A$ est ouvert, lorsque $ \forall x \in E \setminus A, \exists r > 0 \st \bo(x, r) \cap A = \emptyset$.

On appelle \textbf{voisinage} de $a \in E$ toute partie de $E$ qui contient un ouvert contenant $a$.

\paragraph{Propriétés.} Toute union d'ouverts est ouverte, tout union \textit{finie} de fermés est fermée. Toute intersection \textit{finie} d'ouverts est ouverte, tout intersection de fermés est fermée.

\paragraph{} On étend ces définitions pour parler d'ouverts et fermés \textbf{relatifs} d'une partie de $A \subset E$, qui sont l'intersection d'un ouvert ou d'un fermé de $E$ avec $A$. Idem pour les voisinages.

\paragraph{Topologie.} On appelle \textbf{topologie} d'un \kevn{} $E$ l'ensemble de ses ouverts.

Deux normes sont équivalentes \textit{ssi} l'identité est un \textbf{homéophormisme} (bijection continue, de réciproque continue) d'un \kevn{} sur un autre.

Deux normes sont équivalentes \textit{ssi} elles définissent la même topologie.

\paragraph{Démonstration.} Le sens direct exploite le lien entres normes équivalentes et suites convergentes évoqué plus haut. Pour la réciproque, montrer que l'identité est un homéomorphisme.



\subsection{Convexité}

Une partie $A$ d'un \kevn{} $E$ est dite convexe lorsque

$\forall (x, y) \in A^2, [x, y] = \{ (1-t)x + ty \st t \in [0, 1] \} \subset A$.



\subsection{Intérieur et adhérence}

L'\textbf{intérieur} $\mathring{A}$ d'une partie $A$ est l'union de tous les ouverts contenus dans $A$, c'est le plus grand ouvert contenu dans $A$. Si $\mathring{A} = A$, $A$ est ouvert.

L'\textbf{adhérence} $\bar{A}$ d'une partie $A$ est l'intersection de tous les fermés contenant $A$, c'est le plus petit fermé contenant $A$. Si $\bar{A} = A$, $A$ est fermé.

\paragraph{} On peut démontrer les expressions suivantes de l'intérieur et de l'adhérence:

$\mathring{A} = \{ x \in E \st \exists r > 0 \st \bo(x, r) \subset A \}$

$\bar{A} = \{ x \in E \st \forall r > 0, \bo(x, r) \cap A \neq \emptyset \}$.

\paragraph{Démonstration.} La deuxième se fait avec les contraposées.

\paragraph{Caractérisation séquentielle.} Soit $E$ un \kevn{} et $A \subset E$, alors
\begin{enumerate}
	\item $x \in \bar{A}$ \textit{ssi} $x$ est la limite d'une suite de $A^\mathbb{N}$;
	\item $A$ fermé \textit{ssi} $A$ contient les limites de toute suite convergente dans E d'éléments de A.
\end{enumerate}


\subsection{Densité}

Une partie $A \subset E$ est \textbf{dense} dans E lorsque $\bar{A} = E$.

\paragraph{Caractérisation.} $A$ est dense dans $E$ \textit{ssi} $ \forall x \in E, \exists (x_n) \in A^\mathbb{N} \st x_n \to x$. C'est un corollaire des propriétés précédentes.


\subsection{Limite, continuité dans un \texorpdfstring{\kevn}{K-evn}}

Soient $E, F$ deux \kevn{}, $A \subset E$. 

Alors $f: A \mapsto F$ est admet une limite en $a \in A$ lorsque $\exists l \in E \st \forall \varepsilon > 0, \exists \eta > 0 \st f( \bo(a, \eta) \cap A ) \subset \bo(l, \varepsilon)$. 

Ou encore: $\exists l \in E \st \forall \varepsilon > 0, \exists \eta > 0 \st \forall x \in \bo(a, \eta) \cap A, \norm{f(x) - l} \leq \varepsilon$.

$f$ est \textbf{continue} en $a$ lorsque cette limite est $f(a)$.

\paragraph{Caractérisation séquentielle.} $f$ admet une limite en $a \in \bar{A}$ \textit{ssi} $\forall (a_n) \in A^\mathbb{N} \st a_n \to a, (f(a_n))$ converge. \textbf{Absolument fondamental!}

\paragraph{Démonstration.} Pas des plus simples... Pour la réciproque, montrer que le choix de la suite n'est pas important en construisant deux suites et une troisième en alternant les termes de deux suites. Procéder ensuite par l'absurde.

\paragraph{Caractérisation globale.} $f$ est continue sur $A$ \textit{ssi} l'image réciproque de tout ouvert (resp. fermé) de $F$ est un ouvert (resp. fermé) relatif de $A$.


\subsection{Fonctions lipschitziennes}

Soient $(E, N), (E', N')$ deux \kevn{}, $A \subset E$. 
Alors $f: A \mapsto E'$ est \textbf{lipschitzienne} lorsque

$\exists k \in \mathbb{R}_+ \st \forall (x, y) \in A^2, N'(f(x)-f(y)) \leq k N(x-y)$.


\subsection{Continuité uniforme}

Soient $E$ un \kevn{}, $A \subset E$ et $f:A \to F$. On dit que $f$ est \textbf{uniformément continue} sur $A$ lorsque 

$\forall \varepsilon > 0, \exists \eta > 0\st \forall (x, y) \in A^2, \norm{x-y} \leq \eta \Rightarrow \norm{f(x)-f(y)} \leq \varepsilon$.

Toute fonction lipschitzienne sur $A$ est uniformément continue sur $A$.

Toute fonction uniformément continue sur $A$ est continue sur $A$.


\subsection{Compacts}

\paragraph{Théorème de Bolzano-Weierstrass.} De toute suite bornée de réels ou de complexes on peut extraire une suite convergente.

\paragraph{} Soient $E$ un \kevn{} et $A \subset E$. On dit que $A$ est un \textbf{compact} lorsque de toute suite de $A^\mathbb{N}$ on peut extraire une suite convergente dans A.

Tout compact est fermé et borné (la réciproque est vraie en dimension \textbf{finie} et se montre avec une norme quelconque).

\paragraph{Démonstration.} Le caractère fermé est direct par caractérisation séquentielle et le caractère borné se fait par l'absurde, en construisant une suite non bornée, dont on peut extraire une suite qui serait bornée...

\paragraph{Propriété.} Soit $(x_n)$ une suite convergente, alors $\{ x_n, n \in \mathbb{N} \} \cup \{ \lim x_n \}$ est un compact. Ce résultat permet de montrer qu'une fonction est continue sur $E$ \textit{ssi} elle est continue sur tout compact de $E$.


\subsection{Fonctions continues sur un compact}

Quelques propriétés \textbf{essentielles}:
\begin{enumerate}
	\item l'image continue d'un compact est compacte;
	\item une fonction continue sur un compact est bornée, atteint ses bornes;
	\item une fonction continue sur un compact est uniformément continue sur ce compact, c'est le \textbf{théorème de Heine}.
\end{enumerate}

\paragraph{Démonstration.} Le théorème de Heine se montre par l'absurde.


\subsection{Applications linéaires, multilinéaires}
\label{sec:lc}

Soient $E, F$ deux \kevn{} et $u \in \lin(E, F)$. Alors
\begin{align*}
	u \text{ continue en } 0 &\Leftrightarrow u \text{ continue sur } E \\
	&\Leftrightarrow u \text{ uniformément continue sur } E \\
	&\Leftrightarrow u \text{ lipschitzienne } \\
	&\Leftrightarrow \exists M \geq 0 \st \forall x \in E, \norm{u(x)} \leq M \norm{x}.  
\end{align*}

\paragraph{Conséquence.} En particulier, comme on a la deuxième inégalité triangulaire $|\norm{x} - \norm{y}| \leq \norm{x-y}$, la norme est lipschitzienne donc continue. 

\paragraph{} Soient $E_1, \ldots, E_n$ et $F$ $n+1$ \kevn{}, et $f: \prod_{j=1}^n E_j \to F$ une application $n$-linéaire. Alors $f$ est continue \textit{ssi} $\exists M \geq 0 \st \forall (x_1, \ldots, x_n) \in \prod_{j=1}^n E_j, $

$\norm{f(x_1, \ldots, x_n)} \leq M \prod_{j=1}^n \norm{x_j}$.


\subsection{Norme triple}

La \textbf{norme triple} de $u \in \lin_c(E, F)$ (applications linéaires continues), est le réel $\tnorm{u} = \inf \{ M \geq 0 \st \forall x \in E, \norm{u(x)} \leq M \norm{x} \}$. C'est une norme d'algèbre.

Le couple $(\lin_c(E, F), \tnorm{\cdot})$ est une algèbre normée.


\subsection{Cas de la dimension finie}

En dimension finie, \textit{toutes} les normes sont équivalentes.

Un sev d'un espace de dimension finie est fermé.

En dimension finie, toute application \textit{linéaire} est \textit{continue} (il suffit d'exhiber une base et de faire les majorations appropriées).

Une partie est compacte \textit{ssi} elle est fermé et bornée en dimension finie.


\subsection{Connexité par arcs}

Soit $E$ un \kevn{}, $A \subset E$. $A$ est \textbf{connexe par arcs} lorsque 

$\forall (x, y) \in A^2, \exists \varphi \in \cont([0, 1], E)$ telle que $\varphi([0, 1]) \subset A$ et $\varphi(0) = x$ et $\varphi(1) = y$.

Autrement dit, il existe un chemin continu tracé sur $A$ qui joint $x$ et $y$.

\paragraph{Cas de $\mathbb{R}$.} Les connexes par arc de $\mathbb{R}$ sont \textbf{exactement} les intervalles de $\mathbb{R}$.

\paragraph{Propriété.} Quelques propriétés de stabilité:
\begin{enumerate}
	\item soient $(C_i)_{i \in I}$ une famille de connexes par arcs telle que $C_i \cap C_j \neq \emptyset$, alors $\bigcup\limits_{i \in I} C_i$ est connexe par arcs;
	\item tout produit de connexes par arcs est connexe par arcs;
	\item l'image continue d'une partie connexe par arcs est connexe par arcs.
\end{enumerate}

\paragraph{Composantes connexes par arcs.} On peut définir une relation d'équivalence pour $(a, b) \in A^2$ traduisant que l'on puisse joindre $a$ et $b$ par un chemin continu sur $A$. Les classes d'équivalences sont alors appelées composantes connexes par arcs.


\subsection{Séries vectorielles}

Une série vectorielle $\sum u_n$ converge \textit{ssi} la suite $(\sum^n_{k=0} u_k)$ converge, \textit{ssi} chacune des séries coordonnées $\sum u_n^{(j)}$ converge.

On dit qu'une série vectorielle $\sum u_n$ est \textbf{absolument convergente} lorsque la série $\sum \norm{u_n}$ converge.

\paragraph{Dimension finie.} En dimension finie, l'absolue convergence implique la convergence. Il suffit de considérer la norme $\norm{\cdot}_\infty$ pour montrer la convergence absolue de chaque série coordonnée.

\paragraph{Algèbre normée.} Dans une algèbre normée E munie d'une norme sous multiplicative, on établit que pour $a \in E, \norm{a} < 1$, la série géométrique converge absolument et $\sum_{n=0}^{+\infty} a^n = (1 - a)^{-1}$. On le démontre comme dans le cas réel, ou presque. De même, la série exponentielle converge absolument.