\section{Séries entières}

\textit{Séries de la forme $\sum a_n z^n, (a_n) \in \mathbb{C}^\mathbb{N}, z$ une variable complexe.}


\subsection{Lemme d'Abel}

Soit $\sum a_n z^n$ une série entière. Supposons alors qu'il existe $z_0 \in \mathbb{C}^*$ tel que $ \sup\limits_{n \geq 0} |a_n| |z_0|^n < +\infty $ \textit{(c'est-à-dire que la suite $(a_n z_0^n)$ est bornée)}.

Dans ce cas,
\begin{enumerate}
	\item $\sum a_n z^n$ converge normalement sur tout disque $\bar{D}(0, r), 0 \leq r < |z_0|$;
	\item $\sum a_n z^n$ converge absolument sur $D(0, |z_0|)$.
\end{enumerate}

\paragraph{Corollaire.} S'il existe $z_0$ tel que $\sum a_n z_0$ diverge, alors il y a divergence grossière pour tout $z \st |z| > |z_0|$.


\subsection{Rayon de convergence}

Le \textbf{rayon de convergence} $R \in \bar{\mathbb{R}}_+$ d'une série $\sum a_n z^n$ est défini par $R = \sup \{ r \geq 0 \st \sup |a_n| r^n < +\infty \}$.

Le lemme d'Abel permet de décrire le comportement d'une série entière en fonction de ce rayon.

\paragraph{Cas $R=0$.} La série $\sum a_n z^n$ diverge grossièrement en tout $z_0 \neq 0$.

\paragraph{Cas $R \in \mathbb{R}_+^*$.} Il y a
\begin{enumerate}
	\item convergence \textit{normale} sur tout disque $\bar{D}(0, r), 0 \leq r < R$;
	\item convergence \textit{absolue} sur $D(0, R)$;
	\item divergence grossière pour tout $z$ tel que $|z| > R$.
\end{enumerate}

\paragraph{Cas $R = +\infty$.} Il y a
\begin{enumerate}
	\item convergence \textit{normale} sur tout disque $\bar{D}(0, r), r \geq 0$;
	\item convergence \textit{absolue} sur $\mathbb{C}$.
\end{enumerate}


\subsection{Calcul de rayons de convergence}

Soit $\sum a_n z^n$ une série entière, on suppose $\exists l \in \bar{\mathbb{R}}_+ \st \left| \frac{a_{n+1}}{a_n} \right| \to l$. Alors le rayon de convergence $R$ est donné par $R = \frac{1}{l}$.

\paragraph{Démonstration.} On montre ce résultat avec la règle de d'Alembert. On utilise d'ailleurs celle-ci dans certains cas où on ne peut calculer $\left| \frac{a_{n+1}}{a_n} \right|$. On peut d'ailleurs trouver un principe similaire pour la règle de Cauchy.

\paragraph{Comparaison.} Soient $\sum a_n z^n$ et $\sum b_n z^n$ deux séries entières, de rayons $R_a$ et $R_b$.
\begin{enumerate}
	\item ($\exists N \in \mathbb{N} \st \forall n \geq N, |a_n| \leq |b_n|) \Rightarrow R_a \geq R_b$;
	\item $a_n = O(n^\alpha b_n) \Rightarrow R_a \geq R_b$;
	\item $a_n \sim n^\alpha b_n \Rightarrow R_a = R_b$.
\end{enumerate}

\paragraph{Démonstration.} Se rappeler que $(\forall z, |z| < R_b \Rightarrow |z| \leq R_a) \Leftrightarrow R_b \leq R_a$. Pour le deuxième point, exploiter l'espace entre $|z|$ et $R_b$, et d'Alembert pour le terme embêtant.

\paragraph{Opérations.} Le rayon de convergence de la somme ou du produit de Cauchy est supérieur ou égal au minimum des rayons de convergence.


\subsection{Continuité, dérivabilité, intégrabilité}

Soit $\sum a_n z^n$ de rayon de convergence $R > 0$.

La fonction $D(0, R) \to \mathbb{R}, x \mapsto \sum\limits_{n=0}^{+\infty} a_n x^n$ est de classe $\cont{}^\infty$ sur $D(0, R)$. On peut de plus dériver et intégrer termes à termes.


\subsection{Développement en série entière}

Une fonction $f$ est dite \textbf{développable en série entière} au voisinage de 0 lorsqu'il existe $r > 0$ et une série entière $\sum a_n z^n$ de rayon de convergence strictement supérieur à $r$ telle que $\forall z \in D(0, r), f(z) = \sum\limits_{n=0}^{+\infty} a_n z^n$.

Par extension, elle est développable au voisinage de $z_0$ lorsque $u \mapsto f(z_0 + u)$ est développable au voisinage de 0.

\paragraph{Série de Taylor.} Soit $f$ une fonction développable en série entière sur $]-r, r[$. Alors $f$ est de classe $\cont{}^\infty$ et est égale à la somme de sa série de Taylor sur $]-r,r[$: $\forall x \in ]-r, r[, f(x) = \sum\limits_{n=0}^{+\infty} \frac{f^{(n)}(0)}{n!} x^n$. Ce développement est évidemment unique.

\paragraph{Existence du développement.} La fonction $f$ est développable en série entière sur $]-r, r[$ \textit{ssi} $\forall x \in ]-r, r[, \int_0^x \frac{(x-t)^n}{n!} f^{(n+1)}(t) \ud t \to 0$. Cela vient directement de la formule de Taylor reste-intégral. Une condition \textit{suffisante} est alors l'existence de $M$ tel que $\forall x \in ]-r, r[, \forall n \in \mathbb{N}, |f^{(n)}(x)| \leq M$.


\subsection{Développements classiques}

\begin{align*}
	\mathrm{e}^x &= \sum_{n=0}^{+\infty} \frac{x^n}{n!} \quad &(R = +\infty) \\
	\cos x &= \sum_{n=0}^{+\infty} \frac{(-1)^n}{(2n)!} x^{2n} \quad &(R = +\infty) \\
	\sin x &= \sum_{n=0}^{+\infty} \frac{(-1)^n}{(2n+1)!} x^{2n+1} \quad &(R = +\infty) \\
	\cosh x &= \sum_{n=0}^{+\infty} \frac{x^{2n}}{(2n)!} \quad &(R = +\infty) \\
	\sinh x &= \sum_{n=0}^{+\infty} \frac{x^{2n+1}}{(2n+1)!} \quad &(R = +\infty) \\
	\frac{1}{1-x} &= \sum_{n=0}^{+\infty} x^n \quad &(R = 1) \\
	\frac{1}{1+x} &= \sum_{n=0}^{+\infty} (-1)^n x^n \quad &(R = 1) \\
	\ln(1-x) &= -\sum_{n=1}^{+\infty} \frac{x^n}{n} \quad &(R = 1) \\
	\ln(1+x) &= \sum_{n=1}^{+\infty} \frac{(-1)^{n-1} x^n}{n} \quad &(R = 1) \\
	(1+x)^\alpha &= 1 + \sum_{n=1}^{+\infty} \frac{\alpha (\alpha-1) \ldots (\alpha-n+1)}{n!} x^n \quad &(R = 1, +\infty \text{ si } \alpha \in \mathbb{N}) \\
	\arctan x &= \sum_{n=0}^{+\infty} \frac{(-1)^n x^{2n+1}}{2n+1} \quad &(R = 1) \\
\end{align*}


\subsection{Calcul de développement}

Une méthode usuelle pour calculer le développement en série entière d'une fonction $f$ est la suivante: trouver une équation différentielle satisfaite par $f$, et supposer l'existence d'une série entière de rayon de convergence non nul qui soit solution de cete équation. En développant les relations, on peut trouver une expression des coefficients du développement.


\subsection{Fractions rationnelles}

\paragraph{Un développement particulier.} On montre par récurrence le développement suivant (de rayon de convergence égal à 1):
$\frac{1}{(1-z)^{k+1}} = \sum\limits_{n=0}^{+\infty} \binom{n+k}{n} z^n$.

\paragraph{Théorème.} Soit $F$ une fraction rationnelle n'admettant pas 0 comme pôle, alors $F$ est développable en série entière au voisinage de 0 avec un rayon de convergence égal au plus petit module de ses pôles. Cela découle directement du développement précédent.