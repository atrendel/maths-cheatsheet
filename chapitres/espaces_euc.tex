\section{Espaces euclidiens}

\textit{Espaces préhilbertiens réels de dimension finie.}


\subsection{Dual}

Soit $E$ un \kev{}, on appelle \textbf{dual} de $E$ l'espace $\lin(E, \mathbb{K})$ noté $E^*$. C'est l'espace des formes linéaires de $E$. 

Si $E$ est de dimension finie $n$, alors $\dim E = \dim E^*$. En effet, il suffit de considérer l'isomorphisme qui à une forme linéaire associe sa matrice dans une base quelconque.

\paragraph{Théorème de Riesz.} Soit $E$ un espace euclidien, alors $\begin{aligned} E &\to E^* \\ x &\mapsto \scp{x}{\cdot} \end{aligned}$ est un isomorphisme d'espaces vectoriels, c'est-à-dire que pour toute forme linéaire $\varphi \in E^*, \exists!\ a \in E \st \varphi = \scp{a}{\cdot}$.


\subsection{Automorphismes orthogonaux}

Soit $u \in \mathrm{GL}(E)$, on dit que $u$ est un \textbf{automorphisme orthogonal} de $E$ lorsque $\forall (x, y) \in E^2, \scp{u(x)}{y} = \scp{x}{u^{-1}(y)}$. 

L'ensemble des automorphismes orthogonaux de $E$ est un sous-groupe de $(\mathrm{GL}(E), \circ)$, appelé le \textbf{groupe orthogonal} de $E$ et noté $\mathscr{O}(E)$.

\paragraph{Caractérisation.} Soit $u \in \lin(E)$. On a les équivalences 

$u \in \mathscr{O}(E) \Leftrightarrow \forall x \in $E$, \norm{u(x)} = \norm{x} \Leftrightarrow \forall (x, y) \in E^2, \scp{u(x)}{u(y)} = \scp{x}{y}$. 

Ce sont donc les \textbf{symétries} de $E$.

\paragraph{Bases.} Soit $u \in \lin(E)$. On a les équivalences $u \in \mathscr{O}(E)$ \textit{ssi} il existe une \bon{} transformée en \bon{} par u \textit{ssi} toute \bon{} est transformée en \bon{} par u.

\paragraph{Vision matricielle.} Soit $\mathscr{B}$ une \bon{} de $E$, et $M = \mat_\mathscr{B} (u)$.
Alors $u \in \mathscr{O}(E)$ \textit{ssi} ${}^t M M = I_n$. 

Ainsi, on a bien isomorphisme entre matrices orthogonales et endomorphismes orthogonaux.


\subsection{Isométries directes}

Avec la propriété précédente, on montre que $\forall u \in \mathscr{O}(E), \det u \in \{ -1, 1 \}$ (pas de réciproque!).

En outre, l'ensemble appelé \textbf{groupe spécial orthogonal} de $E$ noté $\mathscr{SO}(E) = \{ u \in \mathscr{O}(E) \st \det u = 1 \}$ est un sous-groupe de $\mathscr{O}(E)$, ses éléments sont les \textbf{isométries directes} ou \textbf{rotations}.


\subsection{Propriétés des automorphismes orthogonaux}

Soit $u \in \mathscr{O}(E)$. Alors
\begin{enumerate}
	\item $\Sp(u) \subset \{ -1, 1 \}$, et en cas d'égalité, $E_1(u) \perp E_{-1}(u)$;
	\item si $u$ est diagonalisable, $u$ est une symétrie orthogonale;
	\item $E = \ker (u - \id_E) \overset{\perp}{\oplus} \im (u - \id_E)$.
\end{enumerate}


\subsection{Matrices orthogonales}

On définit de même le sous-groupe des matrices orthogonales $\mathscr{O}_n(\mathbb{R}) = \{ M \in \matspr{n} \st {}^t M M = I_n \}$, et le sous-groupe des matrices orthogonales directes $\mathscr{SO}_n(\mathbb{R}) = \{ M \in \mathscr{O}_n(\mathbb{R}) \st \det M = 1 \}$.

\paragraph{Propriété.} Une matrice est orthogonale \textit{ssi} ses colonnes forment une \bon{}, \textit{ssi} ses lignes forment une \bon{} (pour les espaces adaptés).


\subsection{Endomorphismes symétriques, antisymétriques}

Soit $u \in \lin(E)$. 

On dit que $u$ est \textbf{symétrique} lorsque $\forall (x, y) \in E^2, \scp{u(x)}{y} = \scp{x}{u(y)}$, et \textbf{antisymétrique} lorsque $\forall (x, y) \in E^2, \scp{u(x)}{y} = -\scp{x}{u(y)}$. En fait, ces propriétés entraînent le caractère linéaire de l'application $u: E \to E$!

\paragraph{Exemple du produit vectoriel.} Le produit vectoriel est antisymétrique. On rappelle que le produit vectoriel de $\vec{x}, \vec{y}$ de $\mathbb{R}^3$ est l'unique vecteur $\vec{x} \wedge \vec{y}$ tel que $\scp{\vec{x} \wedge \vec{y}}{\vec{z}} = \det_\mathscr{B} (\vec{x}, \vec{y}, \vec{z})$ dans n'importe quelle \bon{} directe $\mathscr{B}$.

\paragraph{Vision matricielle.} Soit $\mathscr{B}$ une \bon{}, $u \in \lin(E)$ et $A = \mat_\mathscr{B}(u)$.
Alors $u$ est symétrique \textit{ssi} ${}^tA = A$, antisymétrique \textit{ssi} ${}^tA = -A$.

En outre, les espaces des matrices symétriques et antisymétriques $\matsym{}$ et $\mathscr{A}_n(\mathbb{R})$ vérifient

$\matspr{n} = \matsym{} \overset{\perp}{\oplus} \mathscr{A}_n(\mathbb{R})$.

\paragraph{Propriété.} Pour $u \in \mathscr{S}(E)\cup\mathscr{A}(E), E = \ker u \overset{\perp}{\oplus} \im u$.

\paragraph{Caractérisation des endomorphismes antisymétriques.} $u$ est antisymétrique \textit{ssi} $\forall x \in E, \scp{u(x)}{x} = 0$. En conséquence, le spectre de $u$ vérifie $\Sp(u) \subset \{0\}$.


\subsection{Matrices symétriques positives}

Une matrice $M \in \matsym{}$ est 
\begin{enumerate}
	\item \textbf{positive} lorsque $\forall X \in \matspr{n,1}, {}^t XMX \geq 0$;
	\item \textbf{définie positive} lorsque $\forall X \in \matspr{n,1} \backslash \{0\}, {}^t XMX > 0$.
\end{enumerate}

Si $M$ est définie positive, alors c'est la matrice du produit scalaire $(X, Y) \mapsto {}^tXMY$ dans la base canonique de $\matspr{n,1}$.

\paragraph{Notations.} On note $\matsym{+}$ l'espace des matrices symétriques positives, $\matsym{++}$ celui des matrices symétriques définies positives.

\paragraph{Caractérisation.} Soit $M \in \matsym{}$. On a les équivalences
\begin{enumerate}
	\item $M \in \matsym{+}$ (resp. $\matsym{++}$);
	\item $\Sp(M) \subset \mathbb{R}_+$ (resp. $\mathbb{R}_+^*$);
	\item $\exists N \in \matspr{n}$ (resp. $\mathrm{GL}_n(\mathbb{R})$) $\st M = {}^tNN$.
\end{enumerate}


\subsection{Réduction des endomorphismes symétriques}

\paragraph{Lemme.} Soit $u \in \mathscr{S}(E)$, alors $\chi_u$ est \textit{scindé} dans $\mathbb{R}[X]$.

\paragraph{Démonstration.} Établir, pour $X \in \matspc{n,1}\backslash\{0\}$ un vecteur propre de la matrice $M$ de $u$ associé à $\lambda \in \mathbb{C}$, l'égalité ${}^t\bar{X}MX = N \lambda, N > 0$. Passer successivement au conjugué et à la transposée pour montrer $\lambda = \bar{\lambda}$.

\paragraph{Théorème spectral.} Soit $u \in \mathscr{S}(E)$, $u$ est \textit{orthogonalement diagonalisable}! On le montre par récurrence, en exploitant certaines propriétés de stabilité.


\subsection{Réduction simultanée}

\paragraph{Forme bilinéaire symétrique.} Soit $f$ une forme bilinéaire symétrique sur $E$, alors il existe une \bon{} de $E$ qui diagonalise $f$.

\paragraph{Démonstration.} La matrice $A$ de $f$ dans une base quelconque est orthogonalement diagonalisable et s'écrit donc sous la forme $A = PD^tP$. Donc $f(x, y) = {}^t(^tPX) D (^tPY)$, on a bien diagonalisé $f$.

\paragraph{Réduction simultanée.} Soient $A \in \matsym{++}, B \in \matsym{}$. Alors $\exists P \in \mathrm{GL}_n(\mathbb{R}), D$ diagonale, tel que ${}^tPAP = I_n$ et ${}^tPBP = D$.

\paragraph{Démonstration.} $A$ définit un produit scalaire $\varphi_A$, et $B$ définit une forme bilinéaire symétrique $\varphi_B$. Dans l'espace euclidien $(E, \varphi_A)$, on diagonalise $\varphi_B$. $P$ est alors la matrice de passage entre la base canonique et la \bon{} qui diagonalise $\varphi_B$.


\subsection{Classification des automorphismes orthogonaux} 

Soient $u \in \lin(E)$. Alors $u$ est un automorphisme orthogonal \textit{ssi} il existe une \bon{} dans laquelle la matrice de $u$ est diagonale par blocs, les blocs étant de la forme $\begin{pmatrix}\pm 1 \end{pmatrix}$ ou $\begin{pmatrix}\cos \theta & -\sin \theta \\ \sin \theta & \cos \theta \end{pmatrix}$, avec $\theta \in ]-\pi, \pi[\backslash\{0\}$.

\paragraph{Démonstration.} Par récurrence sur la dimension de $E$. On distingue deux cas. Si $\chi_u$ admet une racine (nécessairement $\pm 1$) associée à un vecteur propre $x$, considérer l'orthogonal de $\Vect x$ et lui appliquer l'hypothèse de récurrence. Sinon, on peut trouver un plan $F$ stable par $u$, et on applique l'hypothèse de récurrence à $F$ et $F^\perp$. Ce plan se construit en considérant une valeur propre $\omega \in \mathbb{C}$ de multiplicité $\alpha$, et est donné par $\Vect (x, u(x))$ où $x \in \ker (u^2 - 2 \mathrm{Re}(\omega) u + |\omega|^2 \id_E) = \ker ((u - \omega) (u - \bar{\omega}))^\alpha $.

\paragraph{Étude en dimension 3.} Soit $A \in \matspr{n}, u \in \lin{}(\mathbb{R}^3)$ canoniquement associé à $A$.
\begin{itemize}
	\item Vérifier que $A \in \mathscr{O}_3(\mathbb{R})$.
	\item Si $A$ est symétrique, $u$ est une \textbf{symétrie orthogonale}. Notons $p=\dim E_1(A)$ et $q=\dim E_{-1}(A)$. Alors on a les relations
	
	$\begin{cases}
		p + q = 3 \\
		p - q = \tr A.
	\end{cases}$
	\item Si $A$ n'est pas symétrique, et $\det A = 1$, alors $u$ est une \textbf{rotation} vérifiant $1 + 2 \cos \theta = \tr A$, et d'axe $E_1(A)$.
	\item Si $A$ n'est pas symétrique, et $\det A = -1$, alors $u$ est la composée commutative d'une rotation et d'une symétrie orthogonale ou \textbf{antirotation}, vérifiant $-1 + 2 \cos \theta = \tr A$, et d'axe $E_{-1}(A)$.
\end{itemize}