\section{Intégration sur un intervalle quelconque}

\textit{Dans la suite, $\mathbb{K} = \mathbb{R}$ ou $\mathbb{C}$.}


\subsection{Convergence des intégrales}

Soient $I = ]a, b[, -\infty \geq a < b \leq +\infty$ et $f$ continue par morceaux sur $I$ dans $\mathbb{K}$. L'intégrale $\int_a^b f(t) \ud t$ converge lorsque $x \mapsto \int_x^b f(t) \ud t$ admet une limite dans $\mathbb{K}$ en $a$ et $x \mapsto \int_a^x f(t) \ud t$ admet une limite dans $\mathbb{K}$ en $b$.


\subsection{Intégrales de Riemann}

$\int_1^{+\infty} \frac{\ud t}{t^\alpha}$ converge \textit{ssi} $\alpha > 1, \int_0^1 \frac{\ud t}{t^\alpha}$ converge \textit{ssi} $\alpha < 1$,
et $\int_0^{+\infty} \frac{\ud t}{t^\alpha}$ diverge pour tout $\alpha$!


\subsection{Fonction \texorpdfstring{$\Gamma$}{gamma}}

On définit la fonction $\Gamma$ par $\Gamma(x) = \int_0^{+\infty} \mathrm{e}^{-t} t^{x-1} \ud t$. On peut montrer les résultats suivants:
\begin{enumerate}
	\item elle est définie sur $\mathbb{R}_+^*$;
	\item $\Gamma(1) = 1$;
	\item $\forall x > 0, \Gamma(x+1) = x \Gamma(x)$ et donc $\forall n \in \mathbb{N}, \Gamma(n+1) = n!$.
\end{enumerate}


\subsection{Intégrabilité}

Soit $f$ continue par morceaux sur $I$ dans $\mathbb{K}$, $f$ est \textbf{intégrable} sur $I$ lorsque $\int_I |f(t)| \ud t$ converge, c'est-à-dire $\int_I f(t) \ud t$ converge absolument. 

On note $\mathrm{L}^1(I, \mathbb{K})$ l'ensemble des fonctions intégrables sur $I$. C'est un \kev{}, plus précisément un sev de l'espace des fonctions continues par morceaux, lorsqu'on le munit de la norme de la convergence en moyenne $f \mapsto \int_I |f|$.

On note de même $\mathrm{L}^2(I, \mathbb{K})$ l'ensemble des fonctions de carré intégrable sur $I$. C'est aussi un \kev{}.

\paragraph{Propriété.} La convergence absolue entraîne la convergence simple, on le montre de façon classique avec $f^+$ et $f^-$ dans le cas réel.


\subsection{Parallèle avec les séries} 

On retrouve les mêmes règles de comparaison que pour les séries (voir \ref{sec:reg}), y compris la ``sommation'' des relations de comparaison, adaptée ici aux intégrales. On retrouve aussi la règle de Riemann, en faisant attention au sens des inégalités qui dépend de l'intervalle considéré, ainsi que les intégrales de Bertrand.

Toutes ces règles ne s'appliquent que pour des intégrales de fonctions \textit{positives}!


\subsection{Parallèle avec les intégrales sur un segment}

De même, on retrouve de nombreuses propriétés des intégrales classiques:
\begin{enumerate}
	\item relation de Chasles;
	\item inégalité triangulaire;
	\item si $f$ est continue, positive et d'intégrale nulle, c'est la fonction nulle;
	\item inégalités de Cauchy-Schwarz et de Minkowsky dans $\mathrm{L}^2(I, \mathbb{K})$.
\end{enumerate}


\subsection{Convergence dominée}

Soient $I$ un intervalle, $(f_n) \in \mcx(I, \mathbb{K})^\mathbb{N}$, et $f \in \mcx(I, \mathbb{K})$. On suppose que
\begin{enumerate}
	\item $f_n \to f$ simplement sur $I$;
	\item \textbf{hypothèse de domination:} $\exists \varphi \in \mathrm{L}^1(I, \mathbb{R}_+) \st \forall n \in \mathbb{N}, |f_n| \leq \varphi$.
\end{enumerate}

Alors
\begin{enumerate}
	\item $\forall n \in \mathbb{N}, f_n \in \mathrm{L}^1(I, \mathbb{K})$;
	\item $f \in \mathrm{L}^1(I, \mathbb{K})$;
	\item $\lim\limits_{n\to +\infty} \int_I f_n = \int_I \lim\limits_{n\to +\infty} f_n$.
\end{enumerate}


\subsection{Interversion série intégrale}

Soient $I$ un intervalle, $\sum f_n$ une série de fonctions $f_n \in \mcx(I, \mathbb{K})$, et $f \in \mcx(I, \mathbb{K})$. On suppose que
\begin{enumerate}
	\item $\sum f_n \to f$ simplement sur $I$;
	\item $\forall n \in \mathbb{N}, f_n \in \mathrm{L}^1(I, \mathbb{K})$;
	\item $\sum \int_I |f_n|$ converge.
\end{enumerate}

Alors
\begin{enumerate}
	\item $f \in \mathrm{L}^1(I, \mathbb{K})$;
	\item $\sum \int_I f_n$ converge;
	\item $\int_I \sum\limits_{n=0}^{+\infty} f_n = \sum\limits_{n=0}^{+\infty} \int_I f_n$.
\end{enumerate}


\subsection{Théorème de continuité}

Soient $A \subset \mathbb{R}^n$, $I$ un intervalle de $\mathbb{R}$ et $f: A\times I \to \mathbb{K}$. Supposons que
\begin{enumerate}
	\item $\forall x \in A, f(x, \cdot)$ est continue par morceaux sur I;
	\item $\forall t \in I, f(\cdot, t)$ est continue sur A;
	\item \textbf{domination:} $\exists \varphi \in \mathrm{L}^1(I, \mathbb{R}_+) \st \forall (x, t) \in A\times I, |f(x, t)| \leq \varphi(t)$.
\end{enumerate}

Alors
\begin{enumerate}
	\item $\forall x \in A, f(x, \cdot)$ est intégrable sur $I$;
	\item la fonction $F: \begin{aligned} A &\to \mathbb{K} \\ x &\mapsto \textstyle\int_I f(x, t) \ud t \end{aligned}$ est continue sur A.
\end{enumerate}

\paragraph{Démonstration.} Utiliser continuité séquentielle et convergence dominée.

\paragraph{Extension.} On peut se contenter de la domination sur tout compact inclus dans $A$, comme pour \ref{sec:cont}.


\subsection{Théorème de dérivation}

Soient $A, I$ deux intervalles de $\mathbb{R}$ et $f:A\times I \to \mathbb{K}$. On suppose que
\begin{enumerate}
	\item $\forall x \in A, f(x, \cdot)$ est intégrable sur I;
	\item $\forall t \in I, f(\cdot, t)$ est continue sur A.
\end{enumerate}

En outre, on suppose que $f$ admet une dérivée partielle $\frac{\partial f}{\partial x}$ qui vérifie les hypothèses du \textit{théorème de continuité}:
\begin{enumerate}[resume]
	\item $\forall x \in A, \frac{\partial f}{\partial x}(x, \cdot)$ est continue par morceaux sur I;
	\item $\forall t \in I, \frac{\partial f}{\partial x}(\cdot, t)$ est continue sur A;
	\item \textbf{domination:} $\exists \varphi \in \mathrm{L}^1(I, \mathbb{R}_+) \st \forall (x, t) \in A\times I, |\frac{\partial f}{\partial x}(x, t)| \leq \varphi(t)$.
\end{enumerate}

Alors
\begin{enumerate}
	\item la fonction $F: \begin{aligned} A &\to \mathbb{K} \\ x &\mapsto \textstyle\int_I f(x, t) \ud t \end{aligned}$ est $\cont^1$ sur A;
	\item $\forall x \in A, F'(x) =  \int_I \frac{\partial f}{\partial x}(x, t) \ud t$,
	
	 soit encore $ \frac{\ud}{\ud x} \int_I f(x, t) \ud t =  \int_I  \frac{\partial f}{\partial x}(x, t) \ud t$.
\end{enumerate}

\paragraph{Démonstration.} Revenir à un taux d'accroissement et étudier une suite de fonctions bien choisie. Il faudra utiliser la convergence dominée, la domination s'obtient par l'inégalité des accroissements finis.

\paragraph{Version $\cont^k$.} 

Soient $A, I$ deux intervalles de $\mathbb{R}$ et $f:A\times I \to \mathbb{K}$. On suppose que $f$ admet des dérivées partielles $\frac{\partial^j f}{\partial x^j}$ pour tout $0\leq j\leq k$.

Pour tout $j \in \intr{0,k}$, on suppose que
\begin{enumerate}
	\item $\forall x \in A, \frac{\partial^j f}{\partial x^j}(x, \cdot)$ est intégrable sur $I$;
	\item $\forall t \in I, \frac{\partial^j f}{\partial x^j}(\cdot, t)$ est $\cont^1$ sur $A$.
\end{enumerate} 

On suppose de plus que $\exists \varphi \in \mathrm{L}^1(I, \mathbb{R}_+) \st \forall (x, t) \in A\times I, \left| \frac{\partial^k f}{\partial x^k}(x, t) \right| \leq \varphi(t)$.

Alors
\begin{enumerate}
	\item la fonction $F: \begin{aligned} A &\to \mathbb{K} \\ x &\mapsto \textstyle\int_I f(x, t) \ud t \end{aligned}$ est $\cont^k$ sur A;
	\item $\forall j \in \intr{1, k}, \frac{\ud^j}{\ud x^j} \int_I f(x, t) \ud t =  \int_I  \frac{\partial^j f}{\partial x^j}(x, t) \ud t$.
\end{enumerate}

\paragraph{Extension.} On peut se contenter de la domination sur tout segment inclus dans $A$ dans les deux cas précédents.