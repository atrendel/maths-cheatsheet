\section{Suites et séries de fonctions}


\subsection{Convergence simple et uniforme}

Soient $E, F$ deux \kevn{}, $A$ une partie de $E$ et $(f_n)$ une suite de fonctions $f_n : A \to F$.

La suite $(f_n)$ converge \textbf{simplement} si il existe $f: A \to F$ telle que 

$\forall x \in A, \forall \varepsilon > 0, \exists N \in \mathbb{N} \st \forall n \geq N, \norm{f_n(x) - f} \leq \varepsilon$.

La suite $(f_n)$ converge \textbf{uniformément} si il existe $f: A \to F$ telle que 

$\forall \varepsilon > 0, \exists N \in \mathbb{N} \st \forall n \geq N, \forall x \in A, \norm{f_n(x) - f} \leq \varepsilon$.

La convergence uniforme implique la convergence simple.

\paragraph{Propriété.} La convergence uniforme de la \textit{suite} $(f_n)$ vers $f$ est équivalente à $\norm{f_n - f}_\infty \to 0$. La convergence uniforme de la \textit{série} $\sum f_n$ est équivalente à la convergence uniforme du reste vers $0$.


\subsection{Théorème de continuité}
\label{sec:cont}

Soient $E, F$ deux \kevn{}, $A \subset E$, $a \in A$ et $(f_n)$ une suite de fonctions $f_n : A \to F$.
On suppose que
\begin{enumerate}
	\item $(f_n)$ converge \textit{uniformément} sur $A$ vers $f$ \textit{(ou au voisinage de $a$)};
	\item $\forall n \in \mathbb{N}$, $f_n$ est continue en $a$.
\end{enumerate}

Alors $f$ est continue en $a$.

\paragraph{Démonstration.} À l'aide de l'inégalité triangulaire et de quelques majorations, montrer $\norm{f(x) - f(a)} \to 0$.

\paragraph{Extension.} Il suffit de montrer la convergence uniforme de $(f_n)$ sur \textit{tout compact} contenu dans $A$. \textbf{Attention}, la convergence uniforme sur tout compact de $A$ n'implique \textbf{pas} la convergence uniforme sur $A$.

\paragraph{Démonstration.} Considérer $(a_n)$ une suite convergeant vers $a$ et le compact $\{ a_n, n \in \mathbb{N} \} \cup \{ \lim a_n \}$. Conclure par caractérisation séquentielle de la limite.


\subsection{Théorème de la double limite}

Soient $E, F$ deux \kevn{}, $A \subset E$, $a \in \bar{A}$ et $(f_n)$ une suite de fonctions $f_n : A \to F$.
On suppose que
\begin{enumerate}
	\item $(f_n)$ converge \textit{uniformément} sur $A$ vers $f$;
	\item $\forall n \in \mathbb{N}$, $f_n$ admet une limite $l_n$ en $a$.
\end{enumerate}

Alors
\begin{enumerate}
	\item la suite $(l_n)$ est convergente;
	\item $f$ admet une limite en $a$;
	\item $ \lim\limits_{n \to \infty} \lim\limits_{x \to a} f_n(x) = \lim\limits_{x \to a} \lim\limits_{n \to \infty}  f_n(x)$ .
\end{enumerate}

\paragraph{Démonstration.} Non exigible!


\subsection{Intégration de suites de fonctions}
\label{sec:int}

\paragraph{Premier théorème.} Soient $[a, b] \subset \mathbb{R}$, $(f_n)$ une suite de fonctions $f_n : [a, b] \to \mathbb{R}$.
On suppose que
\begin{enumerate}
	\item $(f_n)$ converge \textit{uniformément} sur $[a, b]$ vers $f$;
	\item $\forall n \in \mathbb{N}$, $f_n \in \cont{}([a, b], \mathbb{R})$.
\end{enumerate}

Alors
\begin{enumerate}
	\item $f$ est continue donc intégrable sur $[a, b]$ \textit{(voir \ref{sec:cont})};
	\item $(\int_a^b f_n(x) \ud x)$ converge et $\lim\limits_{n \to \infty} \int_a^b f_n(x) \ud x = \int_a^b \lim\limits_{n \to \infty} f_n(x) \ud x$.
\end{enumerate}

\paragraph{Second théorème.} Soient $I$ un intervalle de $\mathbb{R}$, $(f_n)$ une suite de fonctions $f_n : I \to \mathbb{R}$.
On suppose que
\begin{enumerate}
	\item $(f_n)$ converge \textit{uniformément} vers $f$ sur tout segment inclus dans $I$;
	\item $\forall n \in \mathbb{N}$, $f_n \in \cont{}(I, \mathbb{R})$.
\end{enumerate}

La fonction $f$ est continue sur $I$ donc intégrable sur tout segment de $I$ \textit{(voir \ref{sec:cont})}.
Notons pour tout $n$ et $a \in I$, $F_n : I \to \mathbb{R}, x \mapsto \int_a^x f_n(t) \ud t$, et $F : I \to \mathbb{R}, x \mapsto \int_a^x f(t) \ud t$.

Alors $F_n \to F$ uniformément sur \textit{tout segment} de $I$.


\subsection{Dérivation de suites de fonctions}

Soient $I$ un intervalle de $\mathbb{R}$, $(f_n)$ une suite de fonctions $f_n : I \to \mathbb{R}$.
On suppose que
\begin{enumerate}
	\item $\forall n \in \mathbb{N}$, $f_n \in \cont{}^1(I, \mathbb{R})$;
	\item $(f_n')$ converge \textit{uniformément} sur tout segment inclus dans $I$;
	\item $\exists a \in I \st (f_n(a))$ converge.
\end{enumerate}

Alors
\begin{enumerate}
	\item $(f_n)$ converge uniformément sur \textit{tout segment} contenu dans $I$;
	\item $f = \lim f_n \in \cont{}^1(I, \mathbb{R})$;
	\item $\left( \lim\limits_{n \to \infty} f_n \right)' = \lim\limits_{n \to \infty} f_n'$.
\end{enumerate}

\paragraph{Démonstration.} Avec le second théorème du \ref{sec:int}.

\paragraph{Version $\cont{}^k$.} Par récurrence, on peut étendre le théorème.

Soient $I$ un intervalle de $\mathbb{R}$, $(f_n)$ une suite de fonctions $f_n : I \to \mathbb{R}$.
On suppose que
\begin{enumerate}
	\item $\forall n \in \mathbb{N}$, $f_n \in \cont{}^k(I, \mathbb{R})$;
	\item $(f_n^{(k)})$ converge \textit{uniformément} sur tout segment inclus dans $I$;
	\item $\forall j \in \intr{0, k-1}, \exists a_j \in I \st (f_n^{(j)}(a_j))$ converge.
\end{enumerate}

Alors
\begin{enumerate}
	\item $\forall j \in \intr{0, k-1}, (f_n^{(j)})$ converge uniformément sur \textit{tout segment} inclus dans $I$;
	\item $f = \lim f_n \in \cont{}^k(I, \mathbb{R})$;
	\item $\forall j \in \intr{0, k-1}, \left( \lim\limits_{n \to \infty} f_n \right)^{(j)} = \lim\limits_{n \to \infty} f_n^{(j)}$.
\end{enumerate}


\subsection{Convergence normale}

Soient $F$ un \kevn{} de dimension finie, $A \subset \mathbb{R}$, et $(f_n)$ une suite de fonctions $f_n : A \to F$. 

La \textit{série} $\sum f_n$ converge \textbf{normalement} sur $A$ lorsque $\sum \norm{f_n}_\infty$ converge sur $A$. Montrer une convergence normale revient donc à majorer $|f_n(t)|$ par le terme général d'une série convergente indépendamment de $t$.

\paragraph{Règle de Weierstrass.} Toute série $\sum f_n$ normalement convergente sur $A$ est absolument, uniformément convergente sur $A$. 

De plus, $ \norm{ \sum\limits_{n=0}^{+\infty} f_n }_\infty \leq \sum\limits_{n=0}^{+\infty} \norm{f_n}_\infty$.


\subsection{Fonction \texorpdfstring{$\zeta$}{zeta} de Riemann}

On définit la \textbf{fonction $\zeta$ de Riemann} par $\zeta : x \mapsto \sum\limits_{n=1}^{+\infty} \frac{1}{n^x}$.

On peut montrer tout un tas de choses sur cette fonction:
\begin{enumerate}
	\item elle est définie sur $]1, +\infty[$;
	\item la série $\sum \frac{1}{n^x}$ converge normalement sur tout intervalle $[a, +\infty[, a > 1$;
	\item $\lim\limits_{x \to +\infty} \zeta(x) = 1$;
	\item $\lim\limits_{x \to 1^+} \zeta(x) = +\infty$;
	\item elle est $\cont{}^\infty$.
\end{enumerate}


\subsection{Approximation des fonctions}

\paragraph{Par des fonctions en escalier.} Soient $F$ un \kevn{} de dimension finie, $f \in \cont{}([a, b], F)$. Alors il existe une suite de fonctions en escalier qui converge uniformément vers $f$ sur $[a, b]$. En d'autres termes, les fonctions en escalier sont denses dans cet espace pour la norme infinie.

\paragraph{Par des polynômes.}  Soit $f \in \cont{}([a, b], \mathbb{K})$. Alors il existe une suite de fonctions polynômiales qui converge uniformément vers $f$ sur $[a, b]$. C'est le \textbf{théorème de Weierstrass}, dont la démonstration est admise. De même, cela traduit la densité des fonctions polynômiales dans cet espace.