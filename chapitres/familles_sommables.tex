\section{Familles sommables}


\subsection{Dénombrabilité}

Un ensemble $X$  est dit \textbf{dénombrable} lorsqu'il est équipotent à $\mathbb{N}$, et \textbf{au plus dénombrable} lorsqu'il est équipotent à une partie de $\mathbb{N}$.

Pour toute partie infinie de $\mathbb{N}$, il existe une bijection \textit{strictement croissante} de $\mathbb{N}$ sur cette partie.

Une partie d'un ensemble au plus dénombrable est également au plus dénombrable.

\paragraph{} Un ensemble $X$ est au plus dénombrable \textit{ssi} $X$ est fini \textit{ou} $X$ est dénombrable.

\paragraph{Résultats classiques.} 
\begin{enumerate}
	\item $\mathbb{Z}$ est dénombrable en considérant l'application qui à $n \in \mathbb{N}$ associe $\frac{n}{2}$ si $n$ est pair, et $-\frac{n+1}{2}$ sinon;
	\item $\mathbb{N}^2$ est dénombrable en considérant la décomposition unique de tout entier non nul sous la forme $n = 2^p (2q+1)$ (et donc $\mathbb{N}^k$ aussi par récurrence); 
	\item $\mathbb{Q}$ est dénombrable en considérant la décomposition de tout élément sous la forme d'une fraction irréductible;
	\item $\mathbb{R}$ n'est \textit{pas} dénombrable (argument diagonal de Cantor).
\end{enumerate}

\paragraph{Produit.} On déduit notamment du caractère dénombrable de $\mathbb{N}^k$ qu'un \textit{produit fini} d'ensembles (au plus) dénombrables est (au plus) dénombrable.

\paragraph{Réunion.} Toute \textit{réunion au plus dénombrable} d'ensembles au plus dénombrables est au plus dénombrable. On le montre en construisant une partition de l'union des ensembles considérés.


\subsection{Nombres algébriques, transcendants}

Un réel ou un complexe est \textbf{algébrique} lorsqu'il est racine d'un polynôme non nul à coefficients dans $\mathbb{Z}$, et \textbf{transcendant} lorsqu'il n'est pas algébrique.

L'ensemble des nombres transcendants n'est pas dénombrable.


\subsection{Suites exhaustives} 

On appelle \textbf{suite exhaustive} de $E$ toute suite $(E_n)_{n\in\mathbb{N}}$ \textit{croissante} telle que $\bigcup_{n\in\mathbb{N}} E_n = E$.

\paragraph{Caractérisation au plus dénombrable.} Un ensemble $E$ est au plus dénombrable \textit{ssi} il existe une suite exhaustive de parties \textit{finies} de $E$.

\paragraph{Une sorte de continuité.} Soient $I$ au plus dénombrable, $(I_n)$ une suite exhaustive de parties \textit{finies} de $I$, $(u_i)_{i\in I}$ une famille de réels positifs. 

Alors $\lim\limits_{n\to\infty} \sum\limits_{i\in I_n} u_i = \sum\limits_{i\in I} u_i$.

Cette propriété intervient dans de nombreuses démonstrations dans la suite.


\subsection{Familles sommables}

Une famille de réels \textit{positifs} $(u_i)_{i\in I}$ est \textbf{sommable} lorsque l'ensemble $\{ \sum_{j\in J} u_j, J \subset I, J \text{ fini} \}$ est majoré. On note $\sum_{i\in I} u_i$ le sup de cet ensemble, qui est éventuellement infini.

\paragraph{Comparaison.} Soient deux familles $(u_i)_{i\in I}$ et $(v_i)_{i\in I}$ avec $\forall i \in I, 0\leq u_i\leq v_i$. Alors
\begin{enumerate}
	\item si $(v_i)$ est sommable, $(u_i)$ sommable;
	\item si $(u_i)$ n'est pas sommable, $(v_i)$ n'est pas sommable;
	\item dans tous les cas, $\sum_I u_i \leq \sum_I v_i$.
\end{enumerate}

\paragraph{Linéarité.} Soient deux familles $(u_i)_{i\in I}$ et $(v_i)_{i\in I}$. La famille $(u_i + v_i)$ est sommable \textit{ssi} les familles $(u_i)$ et $(v_i)$ et dans tous les cas, $\sum_I u_i + v_i = \sum_I u_i + \sum_I v_i$. 

\paragraph{Familles quelconques.} On dit qu'une famille de réels ou de complexes $(u_i)$ est \textbf{sommable} lorsque $(|u_i|)$ est sommable. Les propriétés précédentes s'étendent aisément à ces familles (en considérant parties réelles et imaginaires dans le cas complexe, et avec $u_i^+$ et $u_i^-$ dans le cas réel de signe quelconque).

\paragraph{Lien avec les séries.} La somme d'une série de réels positifs $\sum u_n$ coïncide avec la somme de la famille $(u_n)_{n \in \mathbb{N}}$. Il suffit de considérer la suite exhaustive de parties finies de $\mathbb{N}$ donnée par $(I_n) = (\intr{0, n})$. 

Une famille quelconque indexée par $\mathbb{N}$ est sommable \textit{ssi} la série $\sum u_n$ converge \textit{absolument}.


\subsection{Sommation par paquets}

Soient $I$ au plus dénombrable, $(I_k)_{k\in K}$ une partition de $I$, et $(u_i)_{i\in I}$ une famille de réels positifs.

Alors la famille $(u_i)$ est sommable \textit{ssi} pour tout $k\in K$, $(u_i)_{i\in I_k}$ est sommable, de somme que l'on $S_k$, et que la famille $(S_k)_{k\in K}$ est sommable.

Dans tous les cas, $\sum\limits_{i\in I} u_i = \sum\limits_{k\in K} \Big( \sum\limits_{i\in I_k} u_i \Big)$.


\subsection{Convergence commutative de séries}

\paragraph{Cas des réels positifs.} Soit $(u_n)_{n\in\mathbb{N}}$ une famille, on considère comme avant la suite exhaustive de parties finies de $\mathbb{N}$ notée $(I_n)$ où $I_n = \intr{0, n}$. Pour toute permutation $\sigma$ de $\mathbb{N}$, $(\sigma(I_n))$ est aussi une suite exhaustive de parties finies de $\mathbb{N}$. D'après le résultat établi précédement:

$\sum\limits_{i\in I_n} u_{\sigma(i)} = \sum\limits_{j\in\sigma(I_n)} u_j \to \sum\limits_{j\in\mathbb{N}} u_j$.

En d'autres termes, la série $\sum u_n$ est \textit{commutativement convergente} ou \textit{commutativement divergente}.

\paragraph{Cas des familles quelconques.} Le résultat s'étend aux séries quelconques. Si une série $\sum u_n$ converge \textbf{absolument}, elle est commutativement convergente.


\subsection{Théorème de Fubini}

Soit $(u_{p, q})_{(p, q)\in\mathbb{N}^2}$ une suite double de réels ou de complexes. On a équivalence entre les propriétés suivantes:
\begin{enumerate}
	\item la famille $(u_{p, q})_{(p, q)}$ est sommable;
	\item $\forall p \in \mathbb{N}, (u_{p,q})_{q\geq 0}$ est sommable (ou la série $\sum_{q\geq0} u_{p,q}$ converge absolument);
	\item $\forall q \in \mathbb{N}, (u_{p,q})_{p\geq 0}$ est sommable (ou la série $\sum_{p\geq0} u_{p,q}$ converge absolument);
	\item la série $\sum_{n\geq 0} \Big( \sum_{p+q=n} |u_{p,q}| \Big)$ converge.
\end{enumerate}

Lorsque l'une des propriétés précédentes est réalisée, on a égalité des différentes sommes des séries.

Ce théorème se déduit directement de la sommation par paquets.