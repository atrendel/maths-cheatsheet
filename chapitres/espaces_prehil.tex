\section{Espaces préhilbertiens réels}


\subsection{Formes bilinéaires}

Soit $E$ un $\mathbb{R}$-ev, une application $\varphi$ de $E \times E \to \mathbb{R}$ est \textbf{bilinéaire} lorsque $\forall (x, y) \in E^2, \varphi(x, \cdot)$ et $\varphi(\cdot, y)$ sont linéaires. Elle est \textbf{symétrique} lorsque $\varphi(x, y) = \varphi(y, x)$.

\paragraph{Forme quadratique.} Une \textbf{forme quadratique} est une application $q$ telle que $q(x) = \varphi(x, x)$ où $\varphi$ est une forme bilinéaire symétrique, c'est l'unique forme quadratique associée à $\varphi$.

\paragraph{Identités de polarisation.} $\forall (x, y) \in E^2, \varphi(x, y) = \frac{1}{4}(q(x+y)-q(x-y)$ et $\varphi(x, y) = \frac{1}{2}(q(x+y)-q(x)-q(y))$.

\paragraph{Changement de base.} Soit $\mathscr{B} = (e_1, \ldots, e_n)$ une base de $E$, la matrice $A = ( \varphi(e_i, e_j) )_{1 \leq i, j \leq n}$ est la matrice de la forme bilinéaire symétrique $\varphi$ dans $\mathscr{B}$. Si on note $P$ la matrice de passage vers une base $\mathscr{B}'$, et $A'$ la matrice de $\varphi$ dans $\mathscr{B}'$, alors $A' = {}^tP A P$.


\subsection{Produit scalaire}

Une forme bilinéaire, symétrique et définie positive ($\forall x \in E, \varphi(x, x) \geq 0$ et $\varphi(x, x) = 0 \Rightarrow x = 0$) est un \textbf{produit scalaire}, et on note alors $\varphi(x, y) = \scp{x}{y}$. Le couple $(E, \varphi)$ est alors un \textbf{espace préhilbertien}.

\paragraph{Quelques produits scalaires.} Sur $\matspr{n, p}$, on définit généralement $\scp{A}{B} = \tr({}^tA B)$. Il se calcule en pratique en multipliant coordonnées par coordonnées.

Pour les fonctions adaptées (de carré intégrable ou simplement continues par morceaux selon les cas), $\scp{f}{g} = \int_I f g$ définit un produit scalaire. 

\paragraph{Point de vue matriciel.} Soient $(x, y) \in E$, on pose $X = \mat_{\mathscr{B}}(x), Y = \mat_{\mathscr{B}}(y)$. Soit $A$ la matrice du produit scalaire dans $\mathscr{B}$. 

Alors $\scp{x}{y} = {}^tX A Y$.


\subsection{Inégalités de Cauchy-Schwarz et Minkowski}

\paragraph{Cauchy-Schwarz.} $|\scp{x}{y}| \leq \sqrt{\scp{x}{x}} \sqrt{\scp{y}{y}}$. Il y a égalité \textit{ssi} la famille $(x, y)$ est liée. Soit encore pour la norme euclidienne: $|\scp{x}{y}| \leq \norm{x} \norm{y}$.

\paragraph{Démonstration.} Un classique! Considérer la fonction polynômiale définie par $P:\lambda \mapsto \scp{x + \lambda y}{x + \lambda y}$.

\paragraph{Minkowski.} $\sqrt{\scp{x+y}{x+y}} \leq \sqrt{\scp{x}{x}} + \sqrt{\scp{y}{y}}$. Il y a égalité \textit{ssi} la famille $(x, y)$ est positivement liée. C'est l'inégalité triangulaire, en fait.


\subsection{Orthogonalité}

Deux vecteurs $(x, y) \in E^2$ sont \textbf{orthogonaux} lorsque $\scp{x}{y} = 0$, et on note $x\perp y$. On étend de manière naturelle cette définition aux parties de E.

\paragraph{Quelques propriétés.} \begin{enumerate}
	\item $A^\perp = \Vect(A)^\perp$;
	\item $A^\perp$ est un sev de $E$;
	\item $A \subset B \Rightarrow B^\perp \subset A^\perp$;
	\item $A \subset A^{\perp\perp}$;
	\item $A \perp B \Leftrightarrow A \subset B^\perp \Leftrightarrow B \subset A^\perp$.
\end{enumerate}

\paragraph{Famille orthogonale, orthonormale.} Une famille $(e_i)$ est \textbf{orthogonale} lorsque $\forall i \neq j, \scp{e_i}{e_j} = 0$, \textbf{orthonormale} lorsque $\scp{e_i}{e_j} = \delta_{ij}$.

\paragraph{Théorème de Pythagore.} Si $(x_i)$ est une famille de vecteurs deux à deux orthogonaux, alors $\norm{\sum x_i}^2 = \sum \norm{x_i}^2$. 


\subsection{Supplémentaire orthogonal}

Soit $F$ un sev de $E$, la somme de $F$ et $F^\perp$ est directe. Lorsque $F \oplus F^\perp = E$, $F^\perp$ est le \textbf{supplémentaire orthogonal} de $F$. Dans ce cas, on a l'égalité dans la cinquième propriété précédente.

\paragraph{Dimension finie.} Le supplémentaire orthogonal existe toujours. On peut donc toujours trouver une base orthonormale de $E$, et on peut compléter une famille orthonormale en base orthonormale.

\paragraph{Démonstration.} Il s'agit de montrer que $\dim F^\perp = \dim E - \dim F$, le reste en découle. 

Pour cela, étudier l'application
$\varphi: \begin{aligned} E &\to \mathbb{R}^p \\ x &\mapsto ( \scp{x}{e_1}, \ldots, \scp{x}{e_p} ) \end{aligned}$, où la famille $(e_1, \ldots, e_p)$ est une base de $F$. Le théorème du rang donne une inégalité, l'autre s'obtient avec l'inclusion triviale $F + F^\perp \subset E$.

\paragraph{Dimension quelconque.} Si $F$ est un sev de \textit{dimension finie}, alors il admet un supplémentaire orthogonal, $E$ étant de dimension quelconque.

\paragraph{Démonstration.} Il s'agit de trouver une décomposition de $x \in E$ sur la somme directe $F \oplus F^\perp$. Soit $(e_1, \ldots, e_n)$ une base orthogonale de $F$, on pose alors $y = x - \sum\limits_{i=1}^{n} \frac{\scp{e_i}{x}}{\norm{e_i}^2} e_i$. Il reste à montrer $y \in F^\perp$.


\subsection{Projection orthogonale}

Soit $F$ un sev de $E$ quelconque tel que $E = F \oplus F^\perp$, et $p_F$ la projection orthogonale de $E$ sur $F$ (projection sur $F$ suivant $F^\perp$).

Alors $\forall (x, a) \in F\times E, x = p_F(a) \Leftrightarrow \{ x - a \} \perp F$.

\paragraph{Dimension finie.} Supposons $\dim F < +\infty$. Soit $(e_1, \ldots e_n)$ une base orthonormale de $F$, alors $p_F(x) = \sum\limits_{i=1}^{n} \scp{e_j}{x} e_j$. 


\subsection{Orthonormalisation de Gram-Schmidt}

Soit $(e_i)$ une suite libre d'un espace préhilbertien réel $E$. Alors il existe une suite orthonormale $(\varepsilon_i)$ telle que 

$\forall n \in \mathbb{N}^*, \Vect (e_1, \ldots, e_n) = \Vect (\varepsilon_1, \ldots, \varepsilon_n)$.

On a unicité en rajoutant une condition adéquate, comme par exemple $\forall i, \scp{e_i}{\varepsilon_i} > 0$.

\paragraph{Démonstration.} Avec la famille $(\varepsilon_n) = (\frac{v_n}{\norm{v_n}})$ où $v_n = e_n - \sum\limits_{j=1}^{n-1} \scp{\varepsilon_j}{e_n} \varepsilon_j$. 


\subsection{Caractérisation des projections et symétries}

Soit $p$ un projecteur de $E$, alors
$p$ est un projecteur orthogonal \textit{ssi} $\scp{p(x)}{y} = \scp{x}{p(y)}$ \textit{ssi} $\norm{p(x)} \leq \norm{x}$. 

\paragraph{Démonstration.} Preuve circulaire. L'implication du dernier au premier point se fait par l'absurde, on suppose donc qu'il existe $(x, y) \in \ker p \times \im p \st \scp{x}{y} > 0$. Il reste à poser $z = y - \frac{\scp{x}{y}}{\scp{x}{x}}x$ pour exhiber une contradiction à l'aide de Pythagore.

\paragraph{} Soit $s$ une symétrie de $E$, alors on a
$s$ est une symétrie orthogonale \textit{ssi} $\scp{s(x)}{y} = \scp{x}{s(y)}$ \textit{ssi} $\norm{s(x)} = \norm{x}$ ($s$ est une isométrie).


\subsection{Théorème de la projection orthogonale} 

On rappelle que pour $a \in E$, $d(a, F) = \inf\limits_{x \in F} d(a, x)$.

Soient $E$ un espace préhilbertien réel, $F$ un sev de $E$ et $a \in E$. 

Alors
\begin{enumerate}
	\item l'ensemble $\{ x \in F \st d(a, F) = d(a, x) \}$ possède au plus un élément;
	\item $\forall x \in F, d(a, F) = d(a, x) \Leftrightarrow \{ x - a \} \perp F$;
	\item si $F$ admet un supplémentaire orthogonal, $d(a, F) = d(a, p_F(a))$.
\end{enumerate}

Le point essentiel est le dernier point, qui se démontre aisément pour $\dim F < +\infty$ à l'aide du théorème de Pythagore.


\subsection{Suites totales}

Une partie $A$ de $E$ est \textbf{totale} lorsque $\bar{A} = E$. Une suite $(e_n)$ est une suite totale lorsque $\Vect(e_n, n\geq 0)$ est une partie totale de $E$, c'est-à-dire lorsque \textit{tout élément de $E$ est limite d'une suite de combinaisons linéaires d'éléments de la suite $(e_n)$}.

\paragraph{Inégalité de Bessel.} Soient $E$ un espace préhilbertien réel, et $(e_n)$ une suite orthonormale. Alors $\forall x \in E, \sum\limits_{j=1}^{+\infty} \scp{e_j}{x}^2 \leq \norm{x}^2$.

\paragraph{Démonstration.} Montrer ce résultat pour une famille orthonormale finie $(e_1, \ldots, e_n)$ en considérant le sev de dimension finie $\Vect(e_1, \ldots, e_n)$. Conclure en remarquant que la série $\sum \scp{e_j}{x}^2$ est croissante et majorée.

\paragraph{Convergence de la projection.} Soit $(e_n)$ une suite orthonormale et totale de $E$. Notons $\forall n \in \mathbb{N}, E_n = \Vect(e_0, \ldots, e_n)$ et $p_n$ la projection orthogonale sur $E_n$. Alors $\forall x \in E, p_n(x) \to x$. 

\paragraph{Conséquences.} On en déduit que $x = \sum\limits_{n=0}^{+\infty} \scp{e_n}{x} e_n$ et $\norm{x}^2 = \sum\limits_{n=0}^{+\infty} \scp{e_n}{x}^2$. On retrouve les relations classiques dans une base orthonormale, mais généralisées.